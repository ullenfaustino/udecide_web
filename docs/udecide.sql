-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 17, 2017 at 06:38 PM
-- Server version: 5.7.8-rc
-- PHP Version: 5.6.23-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `udecide`
--

-- --------------------------------------------------------

--
-- Table structure for table `advance_order`
--

CREATE TABLE IF NOT EXISTS `advance_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_code` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `restaurant_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `advance_order_menu`
--

CREATE TABLE IF NOT EXISTS `advance_order_menu` (
  `advance_order_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `item_count` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text,
  `cuisine` text,
  `more_info` text,
  `added_info` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `type`, `cuisine`, `more_info`, `added_info`, `created_at`, `updated_at`) VALUES
(1, 'Cafe', 'Filipino', 'Wifi', 'Masarap', '2017-01-09 12:44:44', '2017-01-09 12:44:44'),
(2, 'Cafe', 'Filipino', 'Wifi', 'nachos', '2017-01-09 12:45:13', '2017-01-09 12:45:13'),
(3, 'Cafe', 'Filipino', 'Wifi', 'Gelato,Ice Cream', '2017-01-09 14:04:17', '2017-01-09 14:04:17'),
(4, 'Cafe', 'Filipino', 'Wifi', 'Accepts Senior citizen card,cafe,meats,match,foods', '2017-01-09 14:18:16', '2017-01-09 14:18:16'),
(5, 'Cafe', 'Filipino', 'Wifi', 'wifi,plaza,plazabrew,coffee,breakfast,breaky,snacks,american', '2017-01-09 14:24:04', '2017-01-09 14:24:04'),
(6, 'Bar,Buffet', 'Filipino', 'Wifi', 'lovekoto,movingon,american,burger,fries,chicken,spaghetti,unligravy', '2017-01-09 14:32:57', '2017-01-09 14:32:57'),
(7, 'Fastfood', 'Italian', 'Senior Citizen Card', 'asd', '2017-01-09 14:46:42', '2017-01-09 14:46:42'),
(8, 'Cafe', 'Filipino', 'Wifi,Senior Citizen Card', 'Garden,Restaurant,Affordable', '2017-01-13 09:52:12', '2017-01-13 09:52:12'),
(9, 'grill', 'Filipino', 'Wifi,Senior Citizen Card', 'burger,barney''s,burgeran,balanga,affordable', '2017-01-13 10:03:04', '2017-01-13 10:03:04'),
(10, 'Cafe', 'Filipino', 'Wifi,Senior Citizen Card', 'sam sam', '2017-01-13 14:20:22', '2017-01-13 14:20:22'),
(11, 'Cafe', 'Italian', 'Wifi', 'pasta,pizza', '2017-01-13 21:58:33', '2017-01-13 21:58:33'),
(12, 'Fastfood', 'Filipino', 'Wifi', 'dasd,asd', '2017-01-13 22:01:20', '2017-01-15 15:14:04');

-- --------------------------------------------------------

--
-- Table structure for table `category_cuisine`
--

CREATE TABLE IF NOT EXISTS `category_cuisine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `category_cuisine`
--

INSERT INTO `category_cuisine` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Filipino', 'Lutong Pinoy', '2017-01-09 02:40:26', '2017-01-09 02:40:26'),
(2, 'Italian', 'Italy', '2017-01-09 14:40:15', '2017-01-09 14:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `category_info`
--

CREATE TABLE IF NOT EXISTS `category_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `category_info`
--

INSERT INTO `category_info` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Wifi', 'wifi', '2017-01-09 02:47:56', '2017-01-09 02:47:56'),
(2, 'Senior Citizen Card', 'Accept', '2017-01-09 14:42:51', '2017-01-09 14:42:51');

-- --------------------------------------------------------

--
-- Table structure for table `category_type`
--

CREATE TABLE IF NOT EXISTS `category_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `description` text,
  `icon_path` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `category_type`
--

INSERT INTO `category_type` (`id`, `name`, `description`, `icon_path`, `created_at`, `updated_at`) VALUES
(1, 'Cafe', 'Cafeteria', 'assets/images/thumbnails/category/6e167cc332644f23cb016bbe39f42ab5.png', '2017-01-09 01:32:04', '2017-01-14 12:50:04'),
(2, 'Bar', 'Bar and Grill', 'assets/images/thumbnails/category/aeb6e2ad222e4a4370d97d24c9063ba1.png', '2017-01-09 02:40:16', '2017-01-14 13:06:32'),
(3, 'Buffet', 'Buffet or Eat all you can', 'assets/images/thumbnails/category/160b267e2d5e9a9144e4233e403a848a.png', '2017-01-09 12:46:31', '2017-01-14 13:06:09'),
(4, 'Fastfood', 'Mabilis na pagkain', 'assets/images/thumbnails/category/3f5711f69826c3c29f6fc7c6b084c363.png', '2017-01-09 14:37:52', '2017-01-14 13:06:16'),
(5, 'grill', 'ihaw-ihaw', 'assets/images/thumbnails/category/f8b88d3af27e582a7edad4d24584fb0f.png', '2017-01-09 14:38:51', '2017-01-14 13:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `checkins`
--

CREATE TABLE IF NOT EXISTS `checkins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `checkins`
--

INSERT INTO `checkins` (`id`, `restaurant_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 17, 2, '2017-01-16 19:05:40', '2017-01-16 19:05:56');

-- --------------------------------------------------------

--
-- Table structure for table `foodie`
--

CREATE TABLE IF NOT EXISTS `foodie` (
  `foodie_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact_no` int(11) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`foodie_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `foodie`
--

INSERT INTO `foodie` (`foodie_id`, `user_id`, `name`, `address`, `contact_no`, `gender`, `email`) VALUES
(1, 10, 'a', 'a', 881, 'Female', 'a@yahoo.com'),
(2, 11, 'a a', 'a', 929381, 'Male', 'aaaa@yahoo.com'),
(3, 12, 'a a', 'ajdjs', 1827, 'Male', 'a@yahoo.com'),
(4, 13, '', '', 0, 'Male', ''),
(5, 14, 'b', 'b ', 9182, 'Male', 'b@yahoo.com'),
(6, 15, 'chrisha enriquez', 'pilar,bataan', 2147483647, 'Female', 'ishaenriquez.24@gmail.com'),
(7, 16, 'anna aranas', 'national road panilao pilar bataab', 2147483647, 'Female', 'anna@yahoo.com'),
(8, 17, 'c c', 'cccc', 82838, 'Male', 'cccc@yahoo.com'),
(9, 18, 'co dad', 'nnqksjx', 29737, 'Male', 'eeee@yahoo.com'),
(10, 19, '', '', 0, 'Male', ''),
(11, 20, 'park bo gum', 'balanga city, bataan', 2147483647, 'Male', 'parkbogum@gmail.com'),
(12, 21, 'heo joon jae', 'balanga city, bataan', 0, 'Male', 'leeminho@gmail.com'),
(13, 24, 'kring elvambs', 'samal, bataan', 2147483647, 'Female', 'kringelvambuena@yahoo.com'),
(14, 28, 'lyra ddnsn', 'xjdjjxjx', 2862, 'Female', 'lyra@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `food_menu`
--

CREATE TABLE IF NOT EXISTS `food_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `food_name` text NOT NULL,
  `description` text NOT NULL,
  `menu_type` varchar(500) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `preparation_time` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `food_menu`
--

INSERT INTO `food_menu` (`id`, `restaurant_id`, `food_name`, `description`, `menu_type`, `price`, `preparation_time`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fried Chicken', 'Crispy Chicken', 'Fried', 89.00, 20.00, '2017-01-09 12:47:42', '2017-01-09 12:47:42'),
(2, 2, 'Nachos', 'Nachos with cheese, beef, tomato, etc.', 'Nachos', 110.00, 8.00, '2017-01-09 13:50:14', '2017-01-09 13:50:14'),
(3, 2, 'Pork Sisig', 'Sisig with pork', 'Lunch', 185.00, 10.00, '2017-01-09 13:52:29', '2017-01-09 13:52:29'),
(4, 2, 'Roast Chicken', 'Quarter Chicken served with rice', 'Dinner', 185.00, 10.00, '2017-01-09 13:54:55', '2017-01-09 13:54:55'),
(5, 2, 'BBQ Ribs', 'Barbecued Ribs served with rice', 'Dinner', 220.00, 10.00, '2017-01-09 13:55:42', '2017-01-09 13:55:42'),
(6, 2, 'Blueberry Cheesecake', 'Cheesecake', 'Dessert', 110.00, 2.00, '2017-01-09 13:56:35', '2017-01-09 13:56:35'),
(7, 7, 'b', 'b', 'b', 1.00, 1.00, '2017-01-09 14:48:42', '2017-01-09 14:48:42'),
(9, 1, 'Chicken with Spagetti', 'Combo meal', 'matamis', 120.00, 20.00, '2017-01-14 00:31:28', '2017-01-14 00:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` varchar(50) NOT NULL DEFAULT '0.00000000',
  `longitude` varchar(50) NOT NULL DEFAULT '0.00000000',
  `location_name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `latitude`, `longitude`, `location_name`) VALUES
(1, '14.676902', '120.535434', 'Balanga City'),
(2, '14.677124', '120.536896', 'Balanga City, Bataan'),
(3, '14.679503', '120.541509', 'Balanga City Bataan'),
(4, '14.678965', '120.540859', 'Balanga City Bataan'),
(5, '14.679669', '120.541292', 'Balanga City Bataan'),
(6, '14.679477', '120.541791', 'Balanga City Bataan'),
(7, '14.679268', '120.540027', 'a'),
(8, '14.676550', '120.533291', 'Balanga City, Bataan'),
(9, '14.684193', '120.539889', 'Balanga City, Bataan'),
(10, '14.674278', '120.512110', 'samsam city'),
(11, '14.716474', '120.536573', 'Abucay, Bataan'),
(12, '14.669824', '120.550712', 'Pilar');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant`
--

CREATE TABLE IF NOT EXISTS `restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `location_id` int(11) DEFAULT '0',
  `resto_name` varchar(100) NOT NULL,
  `owner_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `category_id` int(11) DEFAULT '1',
  `address` varchar(100) NOT NULL,
  `store_hours` varchar(250) DEFAULT '7:00 AM - 10:00 PM',
  `contact_no` varchar(50) NOT NULL,
  `max_capacity` int(11) DEFAULT '0',
  `activation_status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `restaurant`
--

INSERT INTO `restaurant` (`id`, `user_id`, `location_id`, `resto_name`, `owner_name`, `email`, `description`, `category_id`, `address`, `store_hours`, `contact_no`, `max_capacity`, `activation_status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 'Jollibee', 'Jolly Boy', 'jobee@jollibee.ph', 'Langhap Sarap', 1, 'Balanga City', '7:00 AM - 10:00 PM', '1221212', 5, 1, '2017-01-09 12:44:44', '2017-01-14 12:34:45'),
(2, 4, 2, 'Beanery', 'Evelyn M. Bangco', 'beanery@yahoo.com', 'Bean desc', 2, 'Balanga City, Bataan', '8:00 am - 9:00 pm', '237-1233', 0, 1, '2017-01-09 12:45:13', '2017-01-09 12:45:39'),
(3, 5, 3, 'Stregato Gelateria', 'Evelyn M. Bangco', 'stregatogelateria@yahoo.com', 'The only Gelateria in Bataan', 3, 'Balanga City Bataan', '8:00 AM - 9:00 pm', '098765432345', 0, 1, '2017-01-09 14:04:17', '2017-01-09 14:27:31'),
(4, 6, 4, 'Meats and Match', 'Song Joong ki', 'meatsnmatch@gmail.com', 'Meats ', 4, 'Balanga City Bataan', '10:00 am - 10:00 pm', '09989888916', 0, 1, '2017-01-09 14:18:16', '2017-01-09 14:27:05'),
(5, 7, 5, 'Plaza Brew', 'Gong Yoo', 'plazabrew@yahoo.com', 'More on coffee and cakes.', 5, 'Balanga City Bataan', '6:00 am -10:00 pm', '09124578954', 0, 1, '2017-01-09 14:24:04', '2017-01-09 14:27:18'),
(6, 8, 6, 'Mcdonalds', 'Lee Min ho', 'mcdobidaangsaya@gmail.com', 'Love ko ''to', 6, 'Balanga City Bataan', '24 hours', '237-6236', 0, 1, '2017-01-09 14:32:57', '2017-01-13 10:05:32'),
(7, 9, 7, 'a', 'a', 'sada@yahoo.com', 'a', 7, 'a', '8:00 AM - 3:00 pm', '12345', 0, 1, '2017-01-09 14:46:42', '2017-01-12 20:23:03'),
(8, 22, 8, 'Cielo''s Garden and Restaurant', 'Shim Cheong', 'cielosgarden@gmail.com', 'Affordable', 8, 'Balanga City, Bataan', '9:00 am - 10:00 pm', '09172617243', 0, 1, '2017-01-13 09:52:12', '2017-01-13 10:05:07'),
(9, 23, 9, 'Barney'' s Burger', 'Rosset Dela Cruz', 'barneysburger@gmail.com', 'Burger pa more', 9, 'Balanga City, Bataan', '9:00 am - 9:00 pm', '237-0568', 0, 1, '2017-01-13 10:03:04', '2017-01-13 10:10:00'),
(10, 25, 10, 'sam', 'sam masangcap', 'sam@yahoo.com', 'sam', 10, 'samsam city', '8:00 AM - 10:00 pm', '234', 0, 1, '2017-01-13 14:20:22', '2017-01-13 14:21:04'),
(11, 26, 11, 'Felucci', 'Fe Lucci ', 'felucci@yahoo.com', 'Italian', 11, 'Abucay, Bataan', '8:00 AM - 9:00 pm', '098765432345', 0, 1, '2017-01-13 21:58:33', '2017-01-13 22:01:30'),
(12, 27, 12, 'try', 'try', 'rtyr123@yahoo.com', 'try', 12, 'Pilar', '8:00am - 9:00pm', '23424', 15, 1, '2017-01-13 22:01:20', '2017-01-15 09:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_validation`
--

CREATE TABLE IF NOT EXISTS `restaurant_validation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `BrgyBusiness` varchar(100) DEFAULT NULL,
  `zoning` varchar(100) DEFAULT NULL,
  `occupancy` varchar(100) DEFAULT NULL,
  `FireSafety` varchar(100) DEFAULT NULL,
  `CTC` varchar(100) DEFAULT NULL,
  `Sanitary` varchar(100) DEFAULT NULL,
  `DTI` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `restaurant_validation`
--

INSERT INTO `restaurant_validation` (`id`, `restaurant_id`, `BrgyBusiness`, `zoning`, `occupancy`, `FireSafety`, `CTC`, `Sanitary`, `DTI`, `created_at`, `updated_at`) VALUES
(1, 1, 'assets/images/requirements/jollibee/3c6c7093fd421cc93c6cffa7047efc1c.jpg', 'assets/images/requirements/jollibee/3c6c7093fd421cc93c6cffa7047efc1c.jpg', 'assets/images/requirements/jollibee/3c6c7093fd421cc93c6cffa7047efc1c.jpg', 'assets/images/requirements/jollibee/3c6c7093fd421cc93c6cffa7047efc1c.jpg', 'assets/images/requirements/jollibee/3c6c7093fd421cc93c6cffa7047efc1c.jpg', 'assets/images/requirements/jollibee/3c6c7093fd421cc93c6cffa7047efc1c.jpg', 'assets/images/requirements/jollibee/3c6c7093fd421cc93c6cffa7047efc1c.jpg', '2017-01-09 12:44:44', '2017-01-09 12:44:44'),
(2, 2, 'assets/images/requirements/beanery/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/beanery/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/beanery/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/beanery/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/beanery/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/beanery/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/beanery/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-09 12:45:13', '2017-01-09 12:45:13'),
(3, 3, 'assets/images/requirements/stregato/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/stregato/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/stregato/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/stregato/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/stregato/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/stregato/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/stregato/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-09 14:04:17', '2017-01-09 14:04:17'),
(4, 4, 'assets/images/requirements/meatsandmatch/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/meatsandmatch/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/meatsandmatch/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/meatsandmatch/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/meatsandmatch/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/meatsandmatch/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/meatsandmatch/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-09 14:18:16', '2017-01-09 14:18:16'),
(5, 5, 'assets/images/requirements/plazabrew/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/plazabrew/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/plazabrew/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/plazabrew/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/plazabrew/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/plazabrew/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/plazabrew/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-09 14:24:04', '2017-01-09 14:24:04'),
(6, 6, 'assets/images/requirements/mcdo/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/mcdo/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/mcdo/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/mcdo/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/mcdo/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/mcdo/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/mcdo/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-09 14:32:57', '2017-01-09 14:32:57'),
(7, 7, 'assets/images/requirements/a/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/a/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/a/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/a/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/a/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/a/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/a/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-09 14:46:42', '2017-01-09 14:46:42'),
(8, 8, 'assets/images/requirements/cielos/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/cielos/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/cielos/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/cielos/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/cielos/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/cielos/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/cielos/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-13 09:52:12', '2017-01-13 09:52:12'),
(9, 9, 'assets/images/requirements/barneysburger/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/barneysburger/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/barneysburger/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/barneysburger/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/barneysburger/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/barneysburger/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/barneysburger/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-13 10:03:04', '2017-01-13 10:03:04'),
(10, 10, 'assets/images/requirements/sammasangcap/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/sammasangcap/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/sammasangcap/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/sammasangcap/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/sammasangcap/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/sammasangcap/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/sammasangcap/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-13 14:20:22', '2017-01-13 14:20:22'),
(11, 11, 'assets/images/requirements/felucci/a880579063ce76c1a764d070c5042cf7.jpg', 'assets/images/requirements/felucci/27acbba72893b620e5c5501271f5e71f.jpg', 'assets/images/requirements/felucci/91711c44a595c344ed1fc49f3f148c47.jpg', 'assets/images/requirements/felucci/0e1a6beb59983b121fe833d2030a8d7c.jpg', 'assets/images/requirements/felucci/1a389ccb6529f20d49ece64a7bb6ae46.jpg', 'assets/images/requirements/felucci/d92844809bb49ff629ccd284b9b241fe.jpg', 'assets/images/requirements/felucci/cabc3868d443450165a67959cddb21c7.jpg', '2017-01-13 21:58:33', '2017-01-13 21:58:33'),
(12, 12, 'assets/images/requirements/try/fe07eee1f0268775c646e844b05bff2d.png', 'assets/images/requirements/try/d5aaca660e8ae9c023e3fdc116b98757.png', 'assets/images/requirements/try/e882ba5ca4a049219b07b1a5d288b6ad.png', 'assets/images/requirements/try/0aaabbd4c5ffc552e39847fbea1630c1.png', 'assets/images/requirements/try/3481d3bcbde505391d26eeb2b81150fb.png', 'assets/images/requirements/try/3cc4b926391a3ad81d2eae2af3cba061.png', 'assets/images/requirements/try/8a66d05ae652a6a6fa654665fcfb2283.png', '2017-01-13 22:01:20', '2017-01-13 22:01:20');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  `module_type` varchar(255) DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4,
  `ratings` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foodie_id` (`user_id`),
  KEY `resto_id` (`link_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `link_id`, `module_type`, `content`, `ratings`, `created_at`, `updated_at`) VALUES
(1, 15, 1, 'restaurant', 'Sarap ng fried chicken', 4.40, '2017-01-13 17:55:31', '2017-01-13 17:55:31'),
(2, 18, 9, 'restaurant', 'ok', 2.70, '2017-01-13 21:37:11', '2017-01-13 21:37:11'),
(3, 18, 2, 'restaurant', '3', 3.20, '2017-01-13 21:38:29', '2017-01-13 21:38:29'),
(4, 14, 12, 'restaurant', 'jsjsj', 3.60, '2017-01-13 22:33:20', '2017-01-13 22:33:20'),
(5, 15, 2, 'restaurant', 'talap', 3.40, '2017-01-15 01:21:38', '2017-01-15 01:21:38'),
(6, 14, 2, 'restaurant', '4 s', 4.00, '2017-01-15 09:44:03', '2017-01-15 09:44:03'),
(7, 17, 12, 'restaurant', 'kakaj', 2.80, '2017-01-16 18:16:49', '2017-01-16 18:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `seat_reservation`
--

CREATE TABLE IF NOT EXISTS `seat_reservation` (
  `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
  `foodie_id` int(11) NOT NULL,
  `resto_id` int(11) NOT NULL,
  `num_seat` int(11) NOT NULL,
  `sr_status` varchar(100) NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`reservation_id`),
  KEY `resto_id` (`resto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `thumbnails`
--

CREATE TABLE IF NOT EXISTS `thumbnails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) DEFAULT NULL,
  `module_type` varchar(500) DEFAULT NULL,
  `link_id` int(8) DEFAULT '0',
  `is_primary` tinyint(1) DEFAULT '0',
  `is_thumbnail` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `thumbnails`
--

INSERT INTO `thumbnails` (`id`, `file_path`, `module_type`, `link_id`, `is_primary`, `is_thumbnail`, `created_at`, `updated_at`) VALUES
(1, 'assets/images/thumbnails/1/0a5c58fa9589a1b9e8f0fee9a95b2768.png', 'restaurant', 1, 0, 1, '2017-01-09 12:46:50', '2017-01-09 12:46:50'),
(3, 'assets/images/thumbnails/1/b9fb9d37bdf15a699bc071ce49baea53.jpg', 'restaurant', 1, 0, 1, '2017-01-09 12:47:52', '2017-01-09 12:47:52'),
(4, 'assets/images/thumbnails/menu/2/8952e0bc1a7b69af846321525a190e57.jpg', 'food_menu', 2, 0, 1, '2017-01-09 13:50:14', '2017-01-09 13:50:14'),
(5, 'assets/images/thumbnails/menu/3/89021bc2308a6c6d61bf660a0a70d35e.jpg', 'food_menu', 3, 0, 1, '2017-01-09 13:52:47', '2017-01-09 13:52:47'),
(6, 'assets/images/thumbnails/menu/4/cd821a906ace0436105201f31c7d8ed6.jpg', 'food_menu', 4, 0, 1, '2017-01-09 13:54:55', '2017-01-09 13:54:55'),
(7, 'assets/images/thumbnails/menu/5/19f4baa70e618e75ed65ca20f3c2f695.jpg', 'food_menu', 5, 0, 1, '2017-01-09 13:55:42', '2017-01-09 13:55:42'),
(8, 'assets/images/thumbnails/menu/6/849c585f1910770455bfb24b7458992d.jpg', 'food_menu', 6, 0, 1, '2017-01-09 13:56:35', '2017-01-09 13:56:35'),
(9, 'assets/images/thumbnails/menu/7/8e8368b10bbf5f5208a59f2db0d2a0db.jpg', 'food_menu', 7, 0, 1, '2017-01-09 14:48:42', '2017-01-09 14:48:42'),
(10, 'assets/images/thumbnails/10/d110197b3e97c39cdafee72d4c45672b.png', 'restaurant', 10, 0, 1, '2017-01-13 14:21:45', '2017-01-13 14:21:45'),
(11, 'assets/images/thumbnails/12/0aaabbd4c5ffc552e39847fbea1630c1.png', 'restaurant', 12, 0, 1, '2017-01-13 22:03:50', '2017-01-13 22:03:50'),
(12, 'assets/images/thumbnails/1/37625216d2127fe126d9865a3c67b429.jpg', 'restaurant', 1, 0, 1, '2017-01-14 00:29:01', '2017-01-14 00:29:01'),
(13, 'assets/images/thumbnails/menu/9/d4442cf83621149e606df290019cc3f1.jpg', 'food_menu', 9, 0, 1, '2017-01-14 00:31:28', '2017-01-14 00:31:28'),
(14, 'assets/images/thumbnails/menu/9/e7da36e91595c2778061f565bb1c3056.jpg', 'food_menu', 9, 0, 1, '2017-01-14 00:33:36', '2017-01-14 00:33:36'),
(15, 'assets/images/thumbnails/menu/9/54dc0057072fbc9056276417ecdb481c.jpg', 'food_menu', 9, 0, 1, '2017-01-14 00:33:36', '2017-01-14 00:33:36'),
(16, 'assets/images/thumbnails/menu/9/b319cde8919629e61e0918524ad18b27.png', 'food_menu', 9, 0, 1, '2017-01-14 00:33:36', '2017-01-14 00:33:36'),
(17, 'assets/images/thumbnails/menu/1/0690218881af4d837e5686da658e0923.jpg', 'food_menu', 1, 0, 1, '2017-01-14 00:35:37', '2017-01-14 00:35:37'),
(18, 'assets/images/thumbnails/menu/1/6bd5b2d5284e1b2714a7eb1a78257d52.jpg', 'food_menu', 1, 0, 1, '2017-01-14 00:35:37', '2017-01-14 00:35:37'),
(19, 'assets/images/thumbnails/menu/1/f6b0373100d85f80b460a1d993367a93.jpg', 'food_menu', 1, 0, 1, '2017-01-14 00:35:37', '2017-01-14 00:35:37'),
(20, 'assets/images/thumbnails/menu/1/3854baf4a55493762e5cfde927ca62bb.jpg', 'food_menu', 1, 0, 1, '2017-01-14 00:35:37', '2017-01-14 00:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `avatar` varchar(250) DEFAULT 'assets/images/user_avatar/default-avatar.png',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`id`, `user_type`, `username`, `password`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin', 'assets/images/user_avatar/default-avatar.png', '2016-11-29 01:58:34', '2016-11-29 01:58:34'),
(3, 'Restaurant', 'jollibee', 'password', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 12:44:44', '2017-01-09 12:44:44'),
(4, 'Restaurant', 'beanery', 'beanery', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 12:45:13', '2017-01-09 12:45:13'),
(5, 'Restaurant', 'stregato', 'stregatoPW', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:04:17', '2017-01-09 14:04:17'),
(6, 'Restaurant', 'meatsandmatch', 'meatsnmatch', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:18:16', '2017-01-09 14:18:16'),
(7, 'Restaurant', 'plazabrew', 'plazabrew', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:24:04', '2017-01-09 14:24:04'),
(8, 'Restaurant', 'mcdo', 'mcdo', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:32:57', '2017-01-09 14:32:57'),
(9, 'Restaurant', 'a', 'a', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:46:42', '2017-01-09 14:46:42'),
(10, 'Foodie', 'a', 'a', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:49:23', '2017-01-09 14:49:23'),
(11, 'Foodie', 'a', 'a', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:51:35', '2017-01-09 14:51:35'),
(12, 'Foodie', 'a', 'a', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:54:19', '2017-01-09 14:54:19'),
(13, 'Foodie', '', '', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:54:23', '2017-01-09 14:54:23'),
(14, 'Foodie', 'b', 'b', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 14:55:02', '2017-01-09 14:55:02'),
(15, 'Foodie', 'chrishaenriquez', 'pashnea', 'assets/images/user_avatar/default-avatar.png', '2017-01-09 20:10:42', '2017-01-09 20:10:42'),
(16, 'Foodie', 'anna', 'anna', 'assets/images/user_avatar/default-avatar.png', '2017-01-12 11:00:04', '2017-01-12 11:00:04'),
(17, 'Foodie', 'c', 'c', 'assets/images/user_avatar/default-avatar.png', '2017-01-12 11:42:18', '2017-01-12 11:42:18'),
(18, 'Foodie', 'e', 'e', 'assets/images/user_avatar/default-avatar.png', '2017-01-12 11:46:53', '2017-01-12 11:46:53'),
(19, 'Foodie', '', '', 'assets/images/user_avatar/default-avatar.png', '2017-01-12 11:47:00', '2017-01-12 11:47:00'),
(20, 'Foodie', 'parkbogum', 'moonlight', 'assets/images/user_avatar/default-avatar.png', '2017-01-13 09:33:18', '2017-01-13 09:33:18'),
(21, 'Foodie', 'heojoonjae', 'legend', 'assets/images/user_avatar/default-avatar.png', '2017-01-13 09:37:08', '2017-01-13 09:37:08'),
(22, 'Restaurant', 'cielos', 'cielosPW', 'assets/images/user_avatar/default-avatar.png', '2017-01-13 09:52:12', '2017-01-13 09:52:12'),
(23, 'Restaurant', 'barneysburger', 'barneysburgerPW', 'assets/images/user_avatar/default-avatar.png', '2017-01-13 10:03:04', '2017-01-13 10:03:04'),
(24, 'Foodie', 'kringelvambuena', 'kring', 'assets/images/user_avatar/default-avatar.png', '2017-01-13 11:21:30', '2017-01-13 11:21:30'),
(25, 'Restaurant', 'sammasangcap', 'samPW', 'assets/images/user_avatar/default-avatar.png', '2017-01-13 14:20:22', '2017-01-13 14:20:22'),
(26, 'Restaurant', 'felucci', 'felucciPW', 'assets/images/user_avatar/default-avatar.png', '2017-01-13 21:58:33', '2017-01-13 21:58:33'),
(27, 'Restaurant', 'try', 'try', 'assets/images/user_avatar/default-avatar.png', '2017-01-13 22:01:20', '2017-01-13 22:01:20'),
(28, 'Foodie', 'lyra', 'lyra', 'assets/images/user_avatar/default-avatar.png', '2017-01-15 14:47:23', '2017-01-15 14:47:23');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `seat_reservation`
--
ALTER TABLE `seat_reservation`
  ADD CONSTRAINT `seat_reservation_ibfk_1` FOREIGN KEY (`resto_id`) REFERENCES `restaurant` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

class RegistrationController extends BaseController {

    public function registration_view() {
        $this -> data['title'] = 'Registration';
        // $this -> setupDynamicValues();
        $this -> loadCss("login.css");
        $this -> loadJs("validator.js");
        
        $this -> data['category_types'] = CategoryType::all();
        $this -> data['category_cuisines'] = CategoryCuisine::all();
        $this -> data['category_infos'] = CategoryInfo::all();
        View::display('user/registration.twig', $this -> data);
    }
    
    public function registrationRestaurant() {
        try {
            $company_name = Input::post("company_name");
            $owners_name = Input::post("owners_name");
            $description = Input::post("description");
            $address = Input::post("address");
            $store_hours = Input::post("store_hours");
            $phone_number = Input::post("phone_number");
            $max_capacity = Input::post("max_capacity");
            $email = Input::post("email");
            $has_delivery = Input::post("has_delivery");
            
            //categories
            $category_types = Input::post("category_types");
            $category_types = implode(",", $category_types);
            $category_cuisines = Input::post("category_cuisines");
            $category_cuisines = implode(",", $category_cuisines);
            $category_infos = Input::post("category_infos");
            $category_infos = implode(",", $category_infos);
            $additional_infos = Input::post("additional_infos");
            
            $lat = Input::post("lat");
            $lon = Input::post("lon");
            
            $username = Input::post("username");
            $password = Input::post("password");
            $confirm_password = Input::post("confirm_password");
            
            if ($username === $confirm_password) {
                App::flash('message_status', false);
                App::flash('message', 'Password must not be same with Username.');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            }
            
            if ($password !== $confirm_password) {
                App::flash('message_status', false);
                App::flash('message', 'Password does not match.');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            }
            
            $restoLocExist = Restaurant::leftJoin("location as LOC","LOC.id","=","restaurant.location_id")
                                    -> where("resto_name", "=", $company_name)
                                    -> where("latitude","=",$lat)
                                    -> where("longitude", "=", $lon)
                                    -> first();
            if ($restoLocExist) {
                App::flash('message_status', false);
                App::flash('message', 'Unable to add this restaurant, already exist with its location.');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;    
            }
            
            $isUserExist = UserAccount::where("username","=",$username) -> first();
            if ($isUserExist) {
                App::flash('message_status', false);
                App::flash('message', 'Username already exists!');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            }
            
            $barangay_clearance = Input::file("barangay_clearance");
            $zoning_clearance = Input::file("zoning_clearance");
            $occupancy_clearance = Input::file("occupancy_clearance");
            $fire_clearance = Input::file("fire_clearance");
            $cedula = Input::file("cedula");
            $health_clearance = Input::file("health_clearance");
            $dti = Input::file("dti");
            
            $requirements_dir = "assets/images/requirements/" . $username;
            if (!file_exists($requirements_dir)) {
                mkdir($requirements_dir, 0777, TRUE);
            }
            
            $user_account = new UserAccount();
            $user_account -> user_type = "Restaurant";
            $user_account -> username = $username;
            $user_account -> password = md5($confirm_password);
            $user_account -> save();
            
            $geo_location = new Location();
            $geo_location -> latitude = $lat;
            $geo_location -> longitude = $lon;
            $geo_location -> location_name = $address;
            $geo_location -> save(['timestamps' => false]);
            
            $category = new Categories();
            $category -> type = $category_types;
            $category -> cuisine = $category_cuisines;
            $category -> more_info = $category_infos;
            $category -> added_info = $additional_infos;
            $category -> save();
            
            $restaurant = new Restaurant();
            $restaurant -> user_id = $user_account -> id;
            $restaurant -> location_id = $geo_location -> id;
            $restaurant -> resto_name = $company_name;
            $restaurant -> owner_name = $owners_name;
            $restaurant -> email = $email;
            $restaurant -> description = $description;
            $restaurant -> category_id = $category -> id;
            $restaurant -> address = $address;
            $restaurant -> store_hours = $store_hours;
            $restaurant -> contact_no = $phone_number;
            $restaurant -> max_capacity = $max_capacity;
            $restaurant -> has_delivery = $has_delivery;
            $restaurant -> save();
            
            $restaurant_requirements = new RestaurantValidation();
            $restaurant_requirements -> restaurant_id = $restaurant -> id;
            
            // file barangay clearance
            $filename = $barangay_clearance["name"];
            $tmpName = $barangay_clearance["tmp_name"];
            $type = $barangay_clearance["type"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            } else {
                $path = sprintf("%s/%s.%s", $requirements_dir, md5($filename), $ext);
                if (move_uploaded_file($tmpName, $path)) {
                    $restaurant_requirements -> BrgyBusiness = $path;
                }
            }
            
            // Zoning clearance
            $filename = $zoning_clearance["name"];
            $tmpName = $zoning_clearance["tmp_name"];
            $type = $zoning_clearance["type"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            } else {
                $path = sprintf("%s/%s.%s", $requirements_dir, md5($filename), $ext);
                if (move_uploaded_file($tmpName, $path)) {
                    $restaurant_requirements -> zoning = $path;
                }
            }
            
            // occupancy
            $filename = $occupancy_clearance["name"];
            $tmpName = $occupancy_clearance["tmp_name"];
            $type = $occupancy_clearance["type"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            } else {
                $path = sprintf("%s/%s.%s", $requirements_dir, md5($filename), $ext);
                if (move_uploaded_file($tmpName, $path)) {
                    $restaurant_requirements -> occupancy = $path;
                }
            }
            
            // fire_clearance
            $filename = $fire_clearance["name"];
            $tmpName = $fire_clearance["tmp_name"];
            $type = $fire_clearance["type"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            } else {
                $path = sprintf("%s/%s.%s", $requirements_dir, md5($filename), $ext);
                if (move_uploaded_file($tmpName, $path)) {
                    $restaurant_requirements -> FireSafety = $path;
                }
            }
            
            // fire_clearance
            $filename = $cedula["name"];
            $tmpName = $cedula["tmp_name"];
            $type = $cedula["type"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            } else {
                $path = sprintf("%s/%s.%s", $requirements_dir, md5($filename), $ext);
                if (move_uploaded_file($tmpName, $path)) {
                    $restaurant_requirements -> CTC = $path;
                }
            }
            
            // $health_clearance
            $filename = $health_clearance["name"];
            $tmpName = $health_clearance["tmp_name"];
            $type = $health_clearance["type"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            } else {
                $path = sprintf("%s/%s.%s", $requirements_dir, md5($filename), $ext);
                if (move_uploaded_file($tmpName, $path)) {
                    $restaurant_requirements -> Sanitary = $path;
                }
            }
            
            // DTI
            $filename = $dti["name"];
            $tmpName = $dti["tmp_name"];
            $type = $dti["type"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('registration/restaurant'));
                return;
            } else {
                $path = sprintf("%s/%s.%s", $requirements_dir, md5($filename), $ext);
                if (move_uploaded_file($tmpName, $path)) {
                    $restaurant_requirements -> DTI = $path;
                }
            }
            
            $restaurant_requirements -> save();
            
            App::flash('message_status', true);
            App::flash('message', 'Registration successfully submitted to the administrator of this system.');
            Response::redirect($this -> siteUrl('registration/restaurant'));
        } catch(\Exception $e) {
            App::flash('message_status', false);
            App::flash('message', $e -> getMessage());
            Response::redirect($this -> siteUrl('registration/restaurant'));
        }
    }

}

<?php

namespace Admin;

use \App;
use \View;
use \Input;
use \Sentry;
use \Response;

class AdminController extends BaseController {

    /**
     * display the admin dashboard
     */
    public function index() {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }

        // $this -> data['title'] = 'U-Decide';
        // View::display('admin/index2.twig', $this -> data);
        Response::Redirect($this -> siteUrl('/admin/restaurants'));
    }

}

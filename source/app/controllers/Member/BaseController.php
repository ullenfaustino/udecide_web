<?php

namespace Member;

use \App;
use \Menu;
use \Module;
use \Sentry;
use \Response;

class BaseController extends \BaseController {
	
	public function __construct() {
		parent::__construct();
		$this -> data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

		$adminMenu = Menu::create('member_sidebar');

		$user = Sentry::getUser();
		if ($user -> is_registered == 1) {
			foreach (Module::getModules () as $module) {
				$module -> registerMemberMenu();
			}
		} else {
			$dashboard = $adminMenu->createItem('activation', array(
	            'label' => 'Activation',
	            'icon'  => 'dashboard',
	            'url'   => 'member/activation'
	        ));
	
	        $adminMenu->addItem('activation', $dashboard);
	        $adminMenu->setActiveMenu('activation');
		}
	}

}

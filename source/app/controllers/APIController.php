<?php

use \Illuminate\Database\Capsule\Manager as DB;

class APIController extends BaseController {

	public function login() {
		Response::headers() -> set('Content-Type', 'application/json');

		$headers = getallheaders();
		$username = trim($headers["UDecide_USERNAME"]);
		$password = trim($headers["UDecide_PASSWORD"]);

		$account = UserAccount::where("username", "=", $username) -> where("password", "=", $password) -> first();
		if ($account) {
			$response["status"] = "success";
			$response["data"] = $account;
		} else {
			$response["status"] = "error";
			$response["error_message"] = "Invalid Username/Password.";
		}
		Response::setBody(json_encode($response));
	}

	public function registerFoodie() {
		Response::headers() -> set('Content-Type', 'application/json');

		try {
			$username = Input::post('username');
			$password = Input::post('password');
			
			if ($username === $password) {
                $response["status"] = "error";
                $response["message"] = 'Password must not be same with Username.';
                Response::setBody(json_encode($response));
                return;
            }

			$name = Input::post('name');
			$address = Input::post('address');
			$contact_no = Input::post("contact_no");
			$gender = Input::post("gender");
			$email = Input::post("email");
			$foodie_pic = Input::file("foodie_pic");

			$userAccount = new UserAccount();
			$userAccount -> user_type = "Foodie";
			$userAccount -> username = $username;
			$userAccount -> password = md5($password);
			
			// if (Input::hasFile('foodie_pic')) {
			// $filename = $foodie_pic["name"];
			// $tmpName = $foodie_pic["tmp_name"];
			// $type = $foodie_pic["type"];
			//
			// $ext = pathinfo($filename, PATHINFO_EXTENSION);
			// $allowed = array('png', 'PNG', 'jpg', 'JPG');
			// if (!in_array($ext, $allowed)) {
			// $response["status"] = "error";
			// $response["error_message"] = "Invalid File Type.";
			// return;
			// }
			//
			// $foodie_avatar_dir = "assets/images/user_avatar";
			// if (!file_exists($foodie_avatar_dir)) {
			// mkdir($foodie_avatar_dir, 0777, TRUE);
			// }
			//
			// $path = sprintf("%s/%s.%s", $foodie_avatar_dir, md5($filename), $ext);
			//
			// if (move_uploaded_file($tmpName, $path)) {
			// $userAccount -> avatar = $path;
			// }
			// }
			$userAccount -> save();

			$foodie = new Foodie();
			$foodie -> user_id = $userAccount -> id;
			$foodie -> name = $name;
			$foodie -> address = $address;
			$foodie -> contact_no = $contact_no;
			$foodie -> gender = $gender;
			$foodie -> email = $email;
			$foodie -> save(array('timestamps' => false));

			$response["status"] = "success";
			$response["data"] = $foodie;
		} catch(\Exception $e) {
			$response["status"] = "error";
			$response["message"] = $e -> getMessage();
		}

		Response::setBody(json_encode($response));
	}
	
	public function addSeatReservation() {
		Response::headers() -> set('Content-Type', 'application/json');
		try {
		    $codeGen = new CodeGenerator();
		    
            $user_id = Input::post('user_id');
            $restaurant_id = Input::post('restaurant_id');
            $num_seat = Input::post("num_seat");
            $reservation_date = Input::post("reservation_date");
            $additional_request = Input::post("additional_request");
            
            $restaurant = Restaurant::find($restaurant_id);
            $max_capacity = $restaurant -> max_capacity;
            
            $res_date = new \DateTime($reservation_date);
            $curr_date = $res_date -> format("Y-m-d");
            
            $hasCurrentReservation = SeatReservation::where("status","=",0)
            								-> where("restaurant_id", "=", $restaurant_id)
            								-> where("user_id", "=", $user_id)
                                            -> whereRaw(sprintf("DATE(reservation_date) = '%s'", $curr_date))
                                            -> count();
            if ($hasCurrentReservation > 0) {
            	$response["status"] = "error";
                $response["message"] = "You have pending reservation.";
                Response::setBody(json_encode($response));
                return;
            }
            
            $checkins = Checkins::where("restaurant_id", "=", $restaurant_id)
                                -> where("status", "=", 1)
                                -> whereRaw(sprintf("DATE(created_at) = '%s'", $curr_date))
                                -> count();
            
            $curr_reservation = SeatReservation::where("status","=",0)
            								-> where("restaurant_id", "=", $restaurant_id)
                                            -> whereRaw(sprintf("DATE(reservation_date) = '%s'", $curr_date))
                                            -> sum("num_seat");
                                            
            $remaining_seats = $max_capacity - ($checkins + $curr_reservation);
            if ($num_seat > $remaining_seats) {
                $response["status"] = "error";
                $response["message"] = "Not enough available seat.";
            } else {
                $reservation = new SeatReservation();
                $reservation -> user_id = $user_id;
                $reservation -> confirmation_code = $codeGen -> getToken(5);
                $reservation -> restaurant_id = $restaurant_id;
                $reservation -> num_seat = $num_seat;
                $reservation -> reservation_date = $reservation_date;
                $reservation -> additional_request = $additional_request;
                $reservation -> save();
    
                $response["status"] = "success";
                $response["message"] = "Reservation completed.";
                $response["data"] = $reservation;
            }
        } catch(\Exception $e) {
            $response["status"] = "error";
            $response["message"] = $e -> getMessage();
        }                              
		Response::setBody(json_encode($response));
	}
	
	public function cancelReservation() {
		Response::headers() -> set('Content-Type', 'application/json');
		
		$reservation_id = Input::post("reservation_id");
        $remarks = Input::post("remarks");
        
        $reservation = SeatReservation::find($reservation_id);
        if ($reservation) {
            $reservation -> status = 2;
            $reservation -> remarks = $remarks;
            $reservation -> save();
            
            $response["status"] = "success";
            $response["message"] = "Successfully Cancelled.";
        } else {
        	$response["status"] = "error";
            $response["message"] = "Invalid Reservation ID";
        }
        Response::setBody(json_encode($response));
	}
	
	public function addReviews() {
		Response::headers() -> set('Content-Type', 'application/json');

		try {
			$user_id = Input::post('user_id');
			$link_id = Input::post('link_id');
			$module_type = Input::post("module_type");
			$content = Input::post("content");
			$ratings = Input::post("ratings");

			$review = new Reviews();
			$review -> user_id = $user_id;
			$review -> link_id = $link_id;
			$review -> module_type = $module_type;
			$review -> content = $content;
			$review -> ratings = $ratings;
			$review -> save();

			$response["status"] = "success";
			$response["data"] = $review;
		} catch(\Exception $e) {
			$response["status"] = "error";
			$response["message"] = $e -> getMessage();
		}

		Response::setBody(json_encode($response));
	}

	public function addCheckins() {
		Response::headers() -> set('Content-Type', 'application/json');

		try {
			$user_id = Input::post('user_id');
			$restaurant_id = Input::post('restaurant_id');

			$checkin = new Checkins();
			$checkin -> user_id = $user_id;
			$checkin -> restaurant_id = $restaurant_id;
			$checkin -> status = 1;
			$checkin -> save();

			$response["status"] = "success";
			$response["data"] = $checkin;
		} catch(\Exception $e) {
			$response["status"] = "error";
			$response["message"] = $e -> getMessage();
		}

		Response::setBody(json_encode($response));
	}

	public function checkoutRestaurantStatus() {
		Response::headers() -> set('Content-Type', 'application/json');

		try {
			$user_id = Input::post('user_id');
			$restaurant_id = Input::post('restaurant_id');

			$checkins = DB::select(sprintf("SELECT * FROM checkins WHERE `user_id`=%s AND `restaurant_id`=%s AND `status`=1 AND (DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $user_id, $restaurant_id, date('Y-m-d'), date('Y-m-d')));
			$checkins = json_decode(json_encode($checkins));

			foreach ($checkins as $key => $checkin) {
				$updateCheckin = Checkins::find($checkin -> id);
				$updateCheckin -> status = 2;
				$updateCheckin -> save();
			}

			$response["status"] = "success";
			$response["data"] = $checkins;
		} catch(\Exception $e) {
			$response["status"] = "error";
			$response["message"] = $e -> getMessage();
		}

		Response::setBody(json_encode($response));
	}

	public function addAdvanceOrder() {
		Response::headers() -> set('Content-Type', 'application/json');

		$codeGen = new CodeGenerator();

		$user_id = Input::post('user_id');
		$restaurant_id = Input::post('restaurant_id');
		$orders = Input::post('orders');
		$is_delivery = Input::post('is_delivery');
		$delivery_details = Input::post("delivery_details");

		$advanceOrder = new AdvanceOrder();
		$advanceOrder -> ref_code = $codeGen -> getToken(10);
		$advanceOrder -> user_id = $user_id;
		$advanceOrder -> restaurant_id = $restaurant_id;
		$advanceOrder -> is_delivery = $is_delivery;
		$advanceOrder -> save();

		$orders = json_decode($orders);
		$total_price = 0;
		foreach ($orders as $key => $order) {
			$menu = FoodMenu::find($order -> menu_id);
			$total_menu_price = $menu -> price * $order -> item_count;
			$total_price += $total_menu_price;

			$advanceOrderMenu = new AdvanceOrderMenu();
			$advanceOrderMenu -> advance_order_id = $advanceOrder -> id;
			$advanceOrderMenu -> menu_id = $order -> menu_id;
			$advanceOrderMenu -> item_count = $order -> item_count;
			$advanceOrderMenu -> save();
		}

		$advanceOrder = AdvanceOrder::find($advanceOrder -> id);
		$advanceOrder -> total_amount = $total_price;
		$advanceOrder -> save();
		
		if ($is_delivery) {
			$delivery_details = json_decode($delivery_details);
			$delivery = new DeliveryTracking();
			$delivery -> ao_id = $advanceOrder -> id;
			$delivery -> ref_code = $codeGen -> getCodes();
			$delivery -> restaurant_id = $restaurant_id;
			$delivery -> foodie_id = $user_id;
			$delivery -> address = $delivery_details -> address;
			$delivery -> contact_number = $delivery_details -> contact_number;
			$delivery -> save();
		}
		
		Response::setBody(json_encode($advanceOrder));
	}
	
	public function getReservations($user_id) {
	    Response::headers() -> set('Content-Type', 'application/json');
        
        $reservations = SeatReservation::where("user_id","=",$user_id) 
        								-> orderBy("created_at","desc")
        								-> get();
        foreach ($reservations as $key => $reservation) {
            $restaurant = Restaurant::find($reservation -> restaurant_id);
            
            $reservation -> restaurant_name = $restaurant -> resto_name;
            
            $res_date = new \DateTime($reservation -> reservation_date);
            $res_date = $res_date -> format("F d, Y h:i A");
            $reservation -> reservation_date_formatted = $res_date;
            
            $created_at = new \DateTime($reservation -> created_at);
            $created_at = $created_at -> format("F d, Y h:i A");
            $reservation -> created_at_formatted = $created_at;
            
            $reservation -> label_status = ($reservation -> status == 0) ? "Reserved" : (($reservation -> status == 1) ? "End" : "Cancelled");
        }
        
        Response::setBody(json_encode($reservations));
	}
	
	public function getAdvanceOrders($user_id) {
		Response::headers() -> set('Content-Type', 'application/json');
		
		$advanceOrders = AdvanceOrder::leftJoin("restaurant as R","R.id","=","advance_order.restaurant_id")
									// -> where("advance_order.status", "=", 0)
									-> where("advance_order.user_id", "=", $user_id)
									-> select("*", "advance_order.id as id" ,"advance_order.created_at as created_at")
									-> get();
		foreach ($advanceOrders as $key => $advanceOrder) {
			$advanceOrder = $this -> advanceOrderDetails($advanceOrder);
		}
		
		Response::setBody(json_encode($advanceOrders));
	}

	public function getAdvanceOrder($ref_code) {
		Response::headers() -> set('Content-Type', 'application/json');

		$advanceOrder = AdvanceOrder::leftJoin("restaurant as R","R.id","=","advance_order.restaurant_id")
									-> where("advance_order.ref_code", "=", $ref_code) 
									// -> where("advance_order.status", "=", 0)
									-> select("*", "advance_order.created_at as created_at")
									-> first();
		$advanceOrder = $this -> advanceOrderDetails($advanceOrder);
		
		Response::setBody(json_encode($advanceOrder));
	}
	
	private function advanceOrderDetails($advanceOrder) {
		$advanceOrderMenus = AdvanceOrderMenu::leftJoin("food_menu as FM", "FM.id","=","advance_order_menu.menu_id")
											-> where("advance_order_menu.advance_order_id","=",$advanceOrder -> id)
											-> select(["*","FM.id as id"])
											-> get();
		foreach ($advanceOrderMenus as $key => $advanceOrderMenu) {
			$thumbnails = Thumbnails::where("link_id", "=", $advanceOrderMenu -> id) -> where("module_type", "=", "food_menu") -> get();
			$advanceOrderMenu -> thumbnails = $thumbnails;
		}
		$advanceOrder -> orders = $advanceOrderMenus;
		
		//delivery details
		$delivery = DeliveryTracking::leftJoin("foodie as F", "F.user_id","=","delivery_tracking.foodie_id")
									-> where("delivery_tracking.ao_id", "=", $advanceOrder -> id)
									-> select(array("*", "delivery_tracking.id as id", 
													"delivery_tracking.created_at", 
													"delivery_tracking.updated_at",
													"delivery_tracking.address as delivery_address"))
									-> first();
		if ($delivery) {
			$delivery_logs = DeliveryTrackingLogs::where("delivery_tracking_id", "=", $delivery -> id) 
			                                 -> orderBy("created_at","desc")
			                                 -> get();
			$delivery -> delivery_logs = $delivery_logs;
			            
			$advanceOrder -> delivery = $delivery;
		}
		return $advanceOrder;
	}

	public function restaurantDetail($restaurant_id) {
		Response::headers() -> set('Content-Type', 'application/json');

		$headers = getallheaders();
		$user_id = (isset($headers["UDecide_USERID"])) ? trim($headers["UDecide_USERID"]) : null;

		$restaurant = Restaurant::where("activation_status", "=", 1)
								-> where("id" , "=", $restaurant_id)
								-> first();
		$restaurant = $this -> restaurantDetails($restaurant, $user_id);

		Response::setBody(json_encode($restaurant));
	}

	public function restaurantList() {
		Response::headers() -> set('Content-Type', 'application/json');

		$headers = getallheaders();
		$user_id = (isset($headers["UDecide_USERID"])) ? trim($headers["UDecide_USERID"]) : null;

		$restaurants = Restaurant::where("activation_status", "=", 1) -> get();
		foreach ($restaurants as $key => $restaurant) {
			$restaurant = $this -> restaurantDetails($restaurant, $user_id);
		}

		Response::setBody(json_encode($restaurants));
	}

	public function searchRestaurants($keyword) {
		Response::headers() -> set('Content-Type', 'application/json');

		$headers = getallheaders();
		$user_id = (isset($headers["UDecide_USERID"])) ? trim($headers["UDecide_USERID"]) : null;

		$restaurants = Restaurant::leftJoin("categories as CAT", "CAT.id", "=", "restaurant.category_id")
								  -> leftJoin("food_menu as FM", "FM.restaurant_id", "=", "restaurant.id")
								  -> where("activation_status", "=", 1)
								  -> select("*", "restaurant.id as id")
		                          -> whereRaw("resto_name like '%" . $keyword . 
		                          				"%' OR address like '%" . $keyword . 
		                          				"%' OR type like '%" . $keyword . 
		                          				"%' OR cuisine like '%" . $keyword . 
		                          				"%' OR more_info like '%" . $keyword . 
		                          				"%' OR added_info like '%" . $keyword . 
		                          				"%' OR food_name like '%" . $keyword . 
		                          				"%' OR FM.description like '%" . $keyword . 
		                          				"%' OR FM.menu_type like '%" . $keyword . 
		                          				"%'") 
		                          -> get();
		foreach ($restaurants as $key => $restaurant) {
			$restaurant = $this -> restaurantDetails($restaurant, $user_id);
		}

		Response::setBody(json_encode($restaurants));
	}

	public function restaurantMenus($restaurant_id) {
		Response::headers() -> set('Content-Type', 'application/json');

		$headers = getallheaders();
		$user_id = (isset($headers["UDecide_USERID"])) ? trim($headers["UDecide_USERID"]) : null;

		$food_menus = FoodMenu::where("restaurant_id", "=", $restaurant_id) -> get();
		foreach ($food_menus as $key => $food_menu) {
			$thumbnails = Thumbnails::where("link_id", "=", $food_menu -> id) -> where("module_type", "=", "food_menu") -> get();
			$food_menu -> thumbnails = $thumbnails;

			// reviews
			$reviews = Reviews::where("link_id", "=", $food_menu -> id) -> where("module_type", "=", "food_menu") -> get();
			foreach ($reviews as $key => $review) {
				$ua = UserAccount::find($review -> user_id);
				$review -> avatar = $ua -> avatar;
				$review -> username = $ua -> username;
				$review -> ratings = (double)$review -> ratings;
			}
			$reviews_count = count($reviews);
			$sum_reviews = Reviews::where("link_id", "=", $food_menu -> id) -> where("module_type", "=", "food_menu") -> sum("ratings");
			$food_menu -> ratings = ($reviews_count > 0) ? $sum_reviews / $reviews_count : 0;
			$food_menu -> reviews_count = $reviews_count;
			$is_rated = false;
			if (!is_null($user_id)) {
				$user_exist = Reviews::where("link_id", "=", $food_menu -> id) -> where("module_type", "=", "food_menu") -> where("user_id", "=", $user_id) -> first();
				$is_rated = ($user_exist) ? true : false;
			}
			$food_menu -> is_rated = $is_rated;
			$food_menu -> reviews = $reviews;
		}

		Response::setBody(json_encode($food_menus));
	}

	public function categoriesList() {
		Response::headers() -> set('Content-Type', 'application/json');

		$categories = CategoryType::all();

		Response::setBody(json_encode($categories));
	}

	protected function restaurantDetails($restaurant, $user_id = null) {
		// location
		$location = Location::leftJoin("restaurant as Resto", "Resto.location_id", "=", "location.id") -> select(array("location.id as location_id", "location.latitude", "location.longitude", "location.location_name")) -> where("Resto.id", "=", $restaurant -> id) -> first();
		$restaurant -> location = $location;

		// category
		$category = Categories::leftJoin("restaurant as Resto", "Resto.location_id", "=", "categories.id") -> select(array("categories.id as category_id", "categories.type", "categories.cuisine", "categories.more_info", "categories.added_info")) -> where("Resto.id", "=", $restaurant -> id) -> first();
		$restaurant -> category = $category;

		// thumbnails
		$thumbnails = Thumbnails::where("link_id", "=", $restaurant -> id) -> where("module_type", "=", "restaurant") -> get();
		$restaurant -> thumbnails = $thumbnails;

		// default thumbnail$reviews_count
		$thumbnail_count = Thumbnails::where("link_id", "=", $restaurant -> id) -> where("module_type", "=", "restaurant") -> count();
		if ($thumbnail_count > 0) {
			$Primary_thumbnail = Thumbnails::where("link_id", "=", $restaurant -> id) -> where("module_type", "=", "restaurant") -> where("is_primary", "=", 1) -> first();
			if ($Primary_thumbnail) {
				$restaurant -> thumbnail = $Primary_thumbnail;
			} else {
				$arr_thumbnails = json_decode(json_encode($thumbnails), TRUE);
				$restaurant -> thumbnail = $arr_thumbnails[$thumbnail_count - 1];
			}
		}

		// reviews
		$reviews = Reviews::where("link_id", "=", $restaurant -> id) -> where("module_type", "=", "restaurant") -> get();
		foreach ($reviews as $key => $review) {
			$ua = UserAccount::find($review -> user_id);
			$review -> avatar = $ua -> avatar;
			$review -> username = $ua -> username;
			$review -> ratings = (double)$review -> ratings;
		}
		$reviews_count = count($reviews);
		$sum_reviews = Reviews::where("link_id", "=", $restaurant -> id) -> where("module_type", "=", "restaurant") -> sum("ratings");
		$is_rated = false;
		if (!is_null($user_id) && strlen($user_id) > 0) {
			$user_exist = Reviews::where("link_id", "=", $restaurant -> id) -> where("module_type", "=", "restaurant") -> where("user_id", "=", $user_id) -> first();
			$is_rated = ($user_exist) ? true : false;
		}
		$restaurant -> is_rated = $is_rated;
		$restaurant -> ratings = ($reviews_count > 0) ? $sum_reviews / $reviews_count : 0;
		$restaurant -> reviews_count = $reviews_count;
		$restaurant -> reviews = $reviews;
		$restaurant -> has_delivery = ($restaurant -> has_delivery == 1) ? true : false;

		// checkins
		if (!is_null($user_id) && strlen($user_id) > 0) {
			$is_checked_in_today = DB::select(sprintf("SELECT * FROM checkins WHERE `user_id`=%s AND `restaurant_id`=%s AND `status`=1 AND (DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $user_id, $restaurant -> id, date('Y-m-d'), date('Y-m-d')));
			$today_checkins_count_today = DB::select(sprintf("SELECT * FROM checkins WHERE `restaurant_id`=%s AND `status`=1 AND (DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $restaurant -> id, date('Y-m-d'), date('Y-m-d')));
			$total_checkins = Checkins::where("restaurant_id", "=", $restaurant -> id) -> where("status", "=", 2) -> groupBy("user_id") -> count();
			
			$reserved_count = SeatReservation::where("restaurant_id","=",$restaurant -> id)
									-> where("status","=",0)
									-> whereRaw(sprintf("DATE(reservation_date) = '%s'", date("Y-m-d")))
									-> sum("num_seat");
			
			$restaurant -> checkins_today = count($today_checkins_count_today) + $reserved_count;
			$restaurant -> total_checkins = $total_checkins;
			$restaurant -> is_checked_in = (count($is_checked_in_today) > 0) ? true : false;
		} else {
			$restaurant -> checkins_today = 0;
			$restaurant -> total_checkins = 0;
			$restaurant -> is_checked_in = false;
		}
		
		//store status
		if (strpos($restaurant -> store_hours, '-') !== false) {
		    $store_hours = explode("-", $restaurant -> store_hours);
            $curr_time = date("h:i a");
            
            $curr_time = strtotime($curr_time);
            $open = strtotime($store_hours[0]);
            $close = strtotime($store_hours[1]);
            
            if (($curr_time > $open) && ($curr_time < $close)) {
                $store_status = "Open";
            } else {
                $store_status = "Closed";
            }
        } else {
            $store_status = $restaurant -> store_hours;
        }
		
		$restaurant -> store_status = $store_status;
		return $restaurant;
	}

}

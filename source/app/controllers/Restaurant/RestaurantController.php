<?php

namespace Restaurant;

use \App;
use \View;
use \Input;
use \Sentry;
use \Response;

use \Restaurant;

class RestaurantController extends BaseController {

    /**
     * display the admin dashboard
     */
    public function index() {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }
    
        $this -> data['title'] = 'U-Decide';

        $user = json_decode($_SESSION['user']);
        $restaurant = Restaurant::where("user_id", "=", $user -> id) -> where('activation_status', '=', 1) -> first();
        if ($restaurant) {
            Response::redirect($this -> siteUrl('restaurant/dashboard'));
        } else {
            View::display('restaurant/pending_review.twig', $this -> data);
        }
    }

}

<?php

namespace Restaurant;

use \App;
use \View;
use \Menu;
use \Module;
use \Sentry;
use \Response;
use \UserAccount;
use \Restaurant;

class BaseController extends \BaseController {

    public function __construct() {
        parent::__construct();

        // set user session
        if (isset($_SESSION['user'])) {
            $user = json_decode($_SESSION['user']);

            $user = UserAccount::leftJoin("restaurant as Resto", "Resto.user_id", "=", "user_account.id") 
            				-> leftJoin("location as Loc", "Loc.id", "=", "Resto.location_id") 
            				-> leftJoin("categories as Cat", "Cat.id", "=", "Resto.category_id") 
            				-> where("user_account.id","=",$user->id)
            				-> first();
            $this -> data['user'] = $user;
        }
    }

}

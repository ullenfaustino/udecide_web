<?php

class UserController extends BaseController {

    public function index() {
        // App::render('user/index.twig', $this->data);
    }

    public function landingPage() {
        $this -> data['title'] = 'U-Decide';
        // $this -> setupDynamicValues();
        View::display('landing.twig', $this -> data);
        Response::redirect($this -> siteUrl('/login'));
    }

    /**
     * display the login form
     */
    public function login() {
        if (isset($_SESSION['user'])) {
            $user = json_decode($_SESSION['user']);
            if ($user -> user_type === "Admin") {
                Response::redirect($this -> siteUrl('admin'));
            } elseif ($user -> user_type === "Restaurant") {
                Response::redirect($this -> siteUrl('restaurant'));
            } else {
                echo "it is a member's account";
                //TODO: route to members controller
            }
        } else {
            $this -> data['title'] = 'U-Decide';
            // $this -> setupDynamicValues();
            $this -> loadCss("login.css");
            View::display('user/login.twig', $this -> data);
        }
    }

    public function pre_registration_view_per_user($username) {
        if (Sentry::check()) {
            $user = Sentry::getUser();
            if ($user -> hasAnyAccess(array('admin'))) {
                Response::redirect($this -> siteUrl('admin'));
            } else if ($user -> hasAnyAccess(array('cashier'))) {
                Response::redirect($this -> siteUrl('cashier'));
            } else {
                Response::redirect($this -> siteUrl('member'));
            }
        } else {
            $user = Users::where('username', '=', $username) -> first();
            if ($user) {
                $toggleReg = AdminControl::where('name', '=', 'member_registration') -> first();
                $this -> data['toggle_registration'] = $toggleReg;
                $this -> data['title'] = 'Bitomi - Welcome Page';

                $this -> data['generated_username'] = $this -> generateUsername();
                $this -> data['dr_sponsor'] = $username;

                $this -> setupDynamicValues();
            } else {
                Response::redirect($this -> siteUrl("/#login"));
            }

            View::display('landing.twig', $this -> data);
        }
    }

    private function countBAN() {
        $codeGen = new CodeGenerator();
        $uuid = $codeGen -> getToken(12);
        $refid = Users::where('ref_id', '=', $uuid) -> first();
        if (!$refid) {
            return $uuid;
        } else {
            return $this -> countBAN();
        }
    }

    // private function setupDynamicValues() {
    // // landing page
    // $landing_page = GeneralSettings::where('module_name', '=', 'login_attribute') -> first();
    // if ($landing_page) {
    // $this -> data['login_attribute'] = json_decode($landing_page -> content);
    // }
    //
    // // visit us
    // $visit_us = GeneralSettings::where('module_name', '=', 'visit_us') -> first();
    // if ($visit_us) {
    // $this -> data['visit_us'] = json_decode($visit_us -> content);
    // }
    //
    // // contacts
    // $contacts = GeneralSettings::where('module_name', '=', 'contacts') -> first();
    // if ($contacts) {
    // $this -> data['contacts'] = json_decode($contacts -> content);
    // }
    //
    // // about
    // $about = GeneralSettings::where('module_name', '=', 'about') -> first();
    // if ($about) {
    // $this -> data['about'] = json_decode($about -> content);
    // }
    // }

    /**
     * Process the login
     */
    public function doLogin() {
        $remember = Input::post('remember', false);
        $username = Input::post('username');
        $ban = Input::post('ban');
        $redirect = Input::post('redirect');
        $is_ban = Input::post('is_ban');
        $redirect = ($redirect) ? $redirect : ((Input::get('redirect')) ? base64_decode(Input::get('redirect')) : '');

        $username = ($is_ban === "on") ? $ban : $username;

        if (strlen(Input::post('password')) == 0) {
            App::flash('message', "Password Attribute is required");
            Response::redirect($this -> siteUrl("/#login"));
            return;
        }

        try {
            $credential = array('username' => $username, 'password' => Input::post('password'), 'activated' => 1);
            // Try to authenticate the user
            $user = Sentry::authenticate($credential, false);
            if ($remember) {
                Sentry::loginAndRemember($user);
            } else {
                Sentry::login($user, false);
            }

            Response::redirect($this -> siteUrl("/#login"));
        } catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
            if ($is_ban === "on") {
                $user = Users::where("ban", "=", "PH0000-" . $username) -> first();
                if ($user) {
                    try {
                        $username = $user -> username;
                        $credential = array('username' => $username, 'password' => Input::post('password'), 'activated' => 1);
                        // Try to authenticate the user
                        $user = Sentry::authenticate($credential, false);
                        if ($remember) {
                            Sentry::loginAndRemember($user);
                        } else {
                            Sentry::login($user, false);
                        }
                        Response::redirect($this -> siteUrl("/#login"));
                    } catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
                        App::flash('message', $e -> getMessage());
                        App::flash('username', $username);
                        // App::flash('redirect', $redirect);
                        App::flash('remember', $remember);
                        Response::redirect($this -> siteUrl('/'));
                    }
                } else {
                    App::flash('message', "Invalid Business Account Number");
                    Response::redirect($this -> siteUrl("/#login"));
                }
            }

            $user = Sentry::findUserByLogin($username);
            $master = MasterPassword::where('is_active', '=', 1) -> get();
            // Check the password
            if (md5(Input::post('password')) === $master[0] -> password) {
                Sentry::login($user, false);
                Response::redirect($this -> siteUrl("/#login"));
            } else {
                App::flash('message', $e -> getMessage());
                App::flash('username', $username);
                // App::flash('redirect', $redirect);
                App::flash('remember', $remember);
                Response::redirect($this -> siteUrl("/#login"));
            }
        } catch(\Exception $e) {
            if ($is_ban === "on") {
                $user = Users::where("ban", "=", "PH0000-" . $username) -> first();
                if ($user) {
                    try {
                        $username = $user -> username;
                        $credential = array('username' => $username, 'password' => Input::post('password'), 'activated' => 1);
                        // print_r(json_encode($credential));
                        // exit();
                        // Try to authenticate the user
                        $user = Sentry::authenticate($credential, false);
                        if ($remember) {
                            Sentry::loginAndRemember($user);
                        } else {
                            Sentry::login($user, false);
                        }
                        Response::redirect($this -> siteUrl("/#login"));
                    } catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
                        $user = Sentry::findUserByLogin($user -> username);
                        $master = MasterPassword::where('is_active', '=', 1) -> get();
                        // Check the password
                        if (md5(Input::post('password')) === $master[0] -> password) {
                            Sentry::login($user, false);
                            Response::redirect($this -> siteUrl("/#login"));
                        } else {
                            App::flash('message', $e -> getMessage());
                            App::flash('username', $username);
                            // App::flash('redirect', $redirect);
                            App::flash('remember', $remember);
                            Response::redirect($this -> siteUrl("/#login"));
                        }
                    }
                } else {
                    App::flash('message', "Invalid Business Account Number");
                    Response::redirect($this -> siteUrl("/#login"));
                }
            }

            App::flash('message', $e -> getMessage());
            App::flash('username', $username);
            // App::flash('redirect', $redirect);
            App::flash('remember', $remember);
            Response::redirect($this -> siteUrl("/#login"));
        }
    }

    public function doManualLogin() {
        $username = Input::post('username');
        $password = Input::post('password');

        $account = UserAccount::where('username', '=', $username) -> where('password', '=', $password) -> first();
        if ($account) {
            $_SESSION['user'] = json_encode($account);
        } else {
            App::flash('message_status', false);
            App::flash('message', 'Invalid Username/Password');
        }
        Response::redirect($this -> siteUrl("/login"));
    }

    /**
     * Logout the user
     */
    public function logout() {
        // Sentry::logout();
        unset($_SESSION['user']);
        session_destroy();
        Response::redirect($this -> siteUrl("/login"));
    }

    public function forgotPassword() {
        $codegen = new CodeGenerator();
        $username = Input::post('username');
        $pin_code = Input::post('pin_code');

        $user = Users::where('username', '=', $username) -> where('pin_code', '=', $pin_code) -> first();
        if ($user) {
            $resetPassword = $codegen -> getToken(8);

            $user = Sentry::findUserById($user -> id);
            $user -> password = $resetPassword;
            $user -> save();

            $to = $user -> email;
            $subject = "[Bitomi] Password Reset";
            $body = "<p><b>Your password has been successfully reset.</b><p>";
            $body .= "<p>";
            $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
            $body .= "</p>";
            $body .= "<p>";
            $body .= "<span>Password</span>: <b>" . $resetPassword . "</b>";
            $body .= "</p>";
            GenericHelper::sendMail($to, $subject, $body);

            App::flash('message_status', true);
            App::flash('message', "<p>Password successfully reset!</p><p><a target=_blank href=https://mail.google.com>Check your email for your new Password.</a></p>");
        } else {
            App::flash('message', 'Invalid Username and Reference Code');
        }
        Response::redirect($this -> siteUrl('member/deposits'));
    }

    public function sendEmailInquiry() {
        $name = Input::post('name');
        $email = Input::post('email');
        $subject = Input::post('subject');
        $message = Input::post('message');

        $isSent = GenericHelper::sendMailInquiry($name, $email, $subject, $message);
        Response::redirect($this -> siteUrl('/'));
    }

}

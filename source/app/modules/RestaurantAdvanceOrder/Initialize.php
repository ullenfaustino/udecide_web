<?php

namespace RestaurantAdvanceOrder;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

    public function getModuleName() {
        return 'RestaurantAdvanceOrder';
    }

    public function getModuleAccessor() {
    	return 'restaurantadvanceorder';
    }

    public function registerRestaurantMenu() {
    }

    public function registerRestaurantRoute() {
        Route::resource('/advance_order', 'RestaurantAdvanceOrder\Controllers\RestaurantAdvanceOrderController');
        
        Route::get('/advance_order/details/:ref_code', 'RestaurantAdvanceOrder\Controllers\RestaurantAdvanceOrderController:orderDetailsView');
        Route::get('/advance_order/details/delivery/:ao_id', 'RestaurantAdvanceOrder\Controllers\RestaurantAdvanceOrderController:deliveryDetails');
        
        Route::post('/advance_order/update/order', 'RestaurantAdvanceOrder\Controllers\RestaurantAdvanceOrderController:updateOrder') -> name("update_order");
        Route::post('/advance_order/update/delivery', 'RestaurantAdvanceOrder\Controllers\RestaurantAdvanceOrderController:updateDelivery') -> name("update_delivery");
        Route::post('/advance_order/update/pickup', 'RestaurantAdvanceOrder\Controllers\RestaurantAdvanceOrderController:updatePickupDetails') -> name("update_pickup");
    }

}

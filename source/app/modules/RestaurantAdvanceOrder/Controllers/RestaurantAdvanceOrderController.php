<?php

namespace RestaurantAdvanceOrder\Controllers;

use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Restaurant\BaseController;

use \Restaurant;
use \AdvanceOrder;
use \AdvanceOrderMenu;
use \DeliveryTracking;
use \DeliveryTrackingLogs;

class RestaurantAdvanceOrderController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data['active_menu'] = "restaurant_advance_order";

        // validate user session
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        } else {
            $user = json_decode($_SESSION['user']);
            $restaurant = Restaurant::where("user_id", "=", $user -> id) -> where('activation_status', '=', 1) -> first();
            if (!$restaurant) {
                Response::redirect($this -> siteUrl('restaurant'));
            }
        }
        
        $a = [];
    }

    /**
     * display list of resource
     */
    public function index() {
        $user = json_decode($_SESSION['user']);
        $this -> data['title'] = 'Advance Order';

        $restaurant = Restaurant::leftJoin("categories as Cat", "Cat.id", "=", "restaurant.category_id") 
                                -> leftJoin("location as Loc", "Loc.id", "=", "restaurant.location_id") 
                                -> where("user_id", "=", $user -> id) 
                                -> select(array("*", "restaurant.id as id", "Loc.id as location_id", "Cat.id as category_id")) 
                                -> first();
        $this -> data['restaurant'] = $restaurant;

        $advanceOrders = AdvanceOrder::where("restaurant_id", "=", $restaurant -> id) 
                                    -> get();
        foreach ($advanceOrders as $key => $advanceOrder) {
            $menus = AdvanceOrderMenu::leftJoin("food_menu as FM", "FM.id", "=", "advance_order_menu.menu_id") 
                                    -> where("advance_order_id", "=", $advanceOrder -> id) 
                                    -> select("*") 
                                    -> get();
            $advanceOrder -> menus = $menus;
        }
        $this -> data['advance_orders'] = $advanceOrders;
        $this -> data['today'] = date("Y-m-d");
        $this -> data['time_now'] = date("H:i:s");

        View::display('@restaurantadvanceorder/index.twig', $this -> data);
    }

    public function orderDetailsView($ref_code) {
        $this -> data['title'] = 'Advance Order';
        $this -> data["ref_code"] = $ref_code;

        $ao = AdvanceOrder::where("ref_code", "=", $ref_code) -> first();
        $this -> data["aorder"] = $ao;

        $menus = AdvanceOrderMenu::leftJoin("advance_order as AO", "AO.id", "=", "advance_order_menu.advance_order_id") 
                                -> leftJoin("food_menu as FM", "FM.id", "=", "advance_order_menu.menu_id") 
                                -> where("ref_code", "=", $ref_code) 
                                -> select("*") 
                                -> get();
        $this -> data["advance_orders"] = $menus;
        // print_r(json_encode($menus));
        // exit;

        View::display('@restaurantadvanceorder/order_details.twig', $this -> data);
    }
    
    public function deliveryDetails($ao_id) {
         $this -> data['title'] = 'Advance Order - Delivery Details';
         $ao = AdvanceOrder::find($ao_id);
         $this -> data["ref_code"] = $ao -> ref_code;
         
         $delivery_details = DeliveryTracking::where("ao_id","=",$ao_id) -> first();
         $this -> data["delivery_details"] = $delivery_details;
         
         $delivery_logs = DeliveryTrackingLogs::where("delivery_tracking_id", "=", $delivery_details -> id) 
                                            -> orderBy("created_at","desc")
                                            -> get();
         $this -> data["delivery_logs"] = $delivery_logs;
         
         View::display('@restaurantadvanceorder/delivery_details.twig', $this -> data);
    }
    
    public function updateOrder() {
        $status = Input::post("status");
        $ref_code = Input::post("ref_code");
        $ao = AdvanceOrder::where("ref_code", "=", $ref_code) -> first();

        $ao -> status = $status;
        $ao -> save();

        App::flash('message_status', true);
        App::flash('message', "Order Successfully " . (($status == 1) ? "Approved." : "Cancelled."));
        Response::redirect($this -> siteUrl('restaurant/advance_order/details/' . $ref_code));
    }
    
    public function updateDelivery() {
        $user = json_decode($_SESSION['user']);
        
        $delivery_id = Input::post("delivery_id");
        $delivery_name = Input::post("delivery_name");
        $delivery_status = Input::post("delivery_status");
        $remarks = Input::post("remarks");
        
        $delivery = DeliveryTracking::find($delivery_id);
        $delivery -> delivery_name = $delivery_name;
        $delivery -> status = $delivery_status;
        $delivery -> remarks = $remarks;
        $delivery -> updated_by = $user -> id;
        $delivery -> save();
        
        if ($delivery_status == 0) {
            $status = "Still On Queue";
        } elseif ($delivery_status == 1) {
            $status = "Food is on the way";
        } elseif ($delivery_status == 2) {
            $status = "Order is being prepared";
        } elseif ($delivery_status == 3) {
            $status = "Food delivered";
        }
        
        $logs = new DeliveryTrackingLogs();
        $logs -> delivery_tracking_id = $delivery_id;
        $logs -> status = $status;
        $logs -> remarks = $remarks;
        $logs -> save();
        
        App::flash('message_status', true);
        App::flash('message', "Delivery details successfully updated.");
        Response::redirect($this -> siteUrl('restaurant/advance_order/details/delivery/' . $delivery_id));
    }
    
    public function updatePickupDetails() {
        $ao_id = Input::post("ao_id");
        $remarks = Input::post("remarks");
        
        $expected_date = strtr(Input::post("expected_date"), '/', '-');
        $expected_time = Input::post("expected_time");
        $expected_datetime = date("Y-m-d H:i:s", strtotime(sprintf("%s %s", $expected_date, $expected_time)));
        
        $ao = AdvanceOrder::find($ao_id);
        $ao -> expected_time = $expected_datetime;
        $ao -> remarks = $remarks;
        $ao -> save();
        
        App::flash('message_status', true);
        App::flash('message', "Pickup details successfully updated.");
        Response::redirect($this -> siteUrl('restaurant/advance_order'));
    }
}

<?php

namespace AdminCategories;

use \App;
use \Menu;
use \Route;
use \Sentry;
use \ExitHistory;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'AdminCategories';
	}
	public function getModuleAccessor() {
		return 'admincategories';
	}
	public function registerAdminMenu() {
		$user = Sentry::getUser();
		$adminMenu = Menu::get ( 'admin_sidebar' );
		
		// Create Account Details Menu
		$accountMenu = $adminMenu->createItem ( 'admin_categories', array (
				'label' => 'Restaurants',
				'icon' => 'pe-7s-users',
				'url' => 'admin/categories' 
		) );
		// add Parent Menu to Navigation Menu
		$adminMenu->addItem ( 'admin_categories', $accountMenu );
	}
	public function registerAdminRoute() {
		// Route::resource ( '/categories', 'AdminCategories\Controllers\AdminCategoriesController' );
		
		Route::get ( '/categories/type', 'AdminCategories\Controllers\AdminCategoriesController:category_type_view' );
		Route::get ( '/categories/cuisine', 'AdminCategories\Controllers\AdminCategoriesController:category_cuisine_view' );
		Route::get ( '/categories/info', 'AdminCategories\Controllers\AdminCategoriesController:category_info_view' );
		
		Route::post ( '/categories/type/add', 'AdminCategories\Controllers\AdminCategoriesController:setCategoryType' ) -> name("set_category_type");
		Route::post ( '/categories/cuisine/add', 'AdminCategories\Controllers\AdminCategoriesController:setCategoryCuisine' ) -> name("set_category_cuisine");
		Route::post ( '/categories/info/add', 'AdminCategories\Controllers\AdminCategoriesController:setCategoryInfo' ) -> name("set_category_info");
    }
}

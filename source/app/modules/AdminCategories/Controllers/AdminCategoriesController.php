<?php

namespace AdminCategories\Controllers;

use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;

use \CategoryType;
use \CategoryCuisine;
use \CategoryInfo;

class AdminCategoriesController extends BaseController {

    public function __construct() {
        parent::__construct();
        // Menu::get('admin_sidebar') -> setActiveMenu('admin_restaurants');
        $this -> data['active_menu'] = "admin_categories";
    }

    /**
     * display list of resource
     */
    public function index() {
    }

    public function category_type_view() {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }

        $this -> data['title'] = 'Categories';
        $this -> data['active_sub_menu'] = "admin_sub_category_type";

        $category_types = CategoryType::all();
        $this -> data['category_types'] = $category_types;

        View::display('@admincategories/category_type.twig', $this -> data);
    }

    public function category_cuisine_view() {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }

        $this -> data['title'] = 'Categories';
        $this -> data['active_sub_menu'] = "admin_sub_category_cuisine";

        $category_cuisines = CategoryCuisine::all();
        $this -> data['category_cuisines'] = $category_cuisines;

        View::display('@admincategories/category_cuisine.twig', $this -> data);
    }

    public function category_info_view() {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }

        $this -> data['title'] = 'Categories';
        $this -> data['active_sub_menu'] = "admin_sub_category_info";

        $category_infos = CategoryInfo::all();
        $this -> data['category_infos'] = $category_infos;

        View::display('@admincategories/category_info.twig', $this -> data);
    }

    public function setCategoryType() {
        $id = Input::post("id");
        $name = Input::post("name");
        $description = Input::post("description");
        $cat_icon = Input::file("cat_icon");

        $a = new CategoryType();
        if (!is_null($id) && strlen($id) > 0) {
            $a = CategoryType::find($id);
        }

        $a -> name = $name;
        $a -> description = $description;
        if ($cat_icon) {
            $filename = $cat_icon["name"];
            $tmpName = $cat_icon["tmp_name"];
            $type = $cat_icon["type"];

            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('admin/categories/type'));
                return;
            }

            $thumbnails_dir = "assets/images/thumbnails/category";
            if (!file_exists($thumbnails_dir)) {
                mkdir($thumbnails_dir, 0777, TRUE);
            }
            $path = sprintf("%s/%s.%s", $thumbnails_dir, md5($filename), $ext);
            if (move_uploaded_file($tmpName, $path)) {
                if (file_exists($a -> icon_path) && $a -> icon_path !== $path) {
                    unlink($a -> icon_path);
                }
                $a -> icon_path = $path;
            }
        }
        $a -> save();

        App::flash("message_status", true);
        App::flash("message", "Completed.");
        Response::redirect($this -> siteUrl('admin/categories/type'));
    }

    public function setCategoryCuisine() {
        $id = Input::post("id");
        $name = Input::post("name");
        $description = Input::post("description");

        $a = new CategoryCuisine();
        if (!is_null($id) && strlen($id) > 0) {
            $a = CategoryCuisine::find($id);
        }

        $a -> name = $name;
        $a -> description = $description;
        $a -> save();

        App::flash("message_status", true);
        App::flash("message", "Completed.");
        Response::redirect($this -> siteUrl('admin/categories/cuisine'));
    }

    public function setCategoryInfo() {
        $id = Input::post("id");
        $name = Input::post("name");
        $description = Input::post("description");

        $a = new CategoryInfo();
        if (!is_null($id) && strlen($id) > 0) {
            $a = CategoryInfo::find($id);
        }

        $a -> name = $name;
        $a -> description = $description;
        $a -> save();

        App::flash("message_status", true);
        App::flash("message", "Completed.");
        Response::redirect($this -> siteUrl('admin/categories/info'));
    }

}

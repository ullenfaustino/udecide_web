<?php

namespace RestaurantMenu;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

    public function getModuleName() {
        return 'RestaurantMenu';
    }

    public function getModuleAccessor() {
    	return 'restaurantmenu';
    }

    public function registerRestaurantMenu() {
    }

    public function registerRestaurantRoute() {
        Route::resource('/menu', 'RestaurantMenu\Controllers\RestaurantMenuController');
        
        Route::get('/menu/:menu_id/details', 'RestaurantMenu\Controllers\RestaurantMenuController:menuDetails');
        
        Route::post('/menu/upload/thumbnails', 'RestaurantMenu\Controllers\RestaurantMenuController:uploadThumbnails') -> name("upload_menu_thumbnails");
        Route::post('/menu/add', 'RestaurantMenu\Controllers\RestaurantMenuController:addMenu') -> name("add_menu");
        Route::post('/menu/edit', 'RestaurantMenu\Controllers\RestaurantMenuController:editMenu') -> name("edit_menu");
        Route::post('/menu/delete/thumbnail', 'RestaurantMenu\Controllers\RestaurantMenuController:removeMenuThumbnail') -> name("delete_menu_thumbnail");
    }

}

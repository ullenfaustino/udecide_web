<?php

namespace RestaurantMenu\Controllers;

use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Restaurant\BaseController;

use \Restaurant;
use \RestaurantValidation;
use \Thumbnails;
use \FoodMenu;

class RestaurantMenuController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data['active_menu'] = "restaurant_menu";

        // validate user session
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        } else {
            $user = json_decode($_SESSION['user']);
            $restaurant = Restaurant::where("user_id", "=", $user -> id) -> where('activation_status', '=', 1) -> first();
            if (!$restaurant) {
                Response::redirect($this -> siteUrl('restaurant'));
            }
        }
    }

    /**
     * display list of resource
     */
    public function index() {
        $user = json_decode($_SESSION['user']);
        $this -> data['title'] = 'Menu';

        $restaurant = Restaurant::leftJoin("categories as Cat", "Cat.id", "=", "restaurant.category_id") -> leftJoin("location as Loc", "Loc.id", "=", "restaurant.location_id") -> where("user_id", "=", $user -> id) -> select(array('*', 'restaurant.id as id')) -> first();
        $this -> data['restaurant'] = $restaurant;

        $menus = FoodMenu::where("restaurant_id", "=", $restaurant -> id) -> get();
        foreach ($menus as $key => $menu) {
            $thumbnails = Thumbnails::where("link_id", "=", $menu -> id) -> where("module_type", "=", "food_menu") -> get();
            $menu -> thumbnails = $thumbnails;
        }
        $this -> data['menus'] = $menus;

        View::display('@restaurantmenu/index.twig', $this -> data);
    }

    public function menuDetails($menu_id) {
        $user = json_decode($_SESSION['user']);
        $this -> data['title'] = 'Menu Details';

        $menu = FoodMenu::find($menu_id);
        $this -> data['menu'] = $menu;

        $thumbnails = Thumbnails::where("link_id", "=", $menu -> id) -> where("module_type", "=", "food_menu") -> get();
        $this -> data['thumbnails'] = $thumbnails;

        View::display('@restaurantmenu/details.twig', $this -> data);
    }

    public function uploadThumbnails() {
        $menu_id = Input::post('menu_id');
        $thumbnails = Input::file('thumbnails');

        $thumbnailsLength = count($thumbnails["name"]);
        if ($thumbnailsLength > 0) {
            for ($i = 0; $i < $thumbnailsLength; $i++) {
                $filename = $thumbnails["name"][$i];
                $tmpName = $thumbnails["tmp_name"][$i];
                $type = $thumbnails["type"][$i];

                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                $allowed = array('png', 'PNG', 'jpg', 'JPG');
                if (!in_array($ext, $allowed)) {
                    App::flash('message_status', false);
                    App::flash('message', 'Invalid File Type');
                    Response::redirect($this -> siteUrl('restaurant/menu/' . $menu_id . "/details"));
                    return;
                }

                $thumbnails_dir = "assets/images/thumbnails/menu/" . $menu_id;
                if (!file_exists($thumbnails_dir)) {
                    mkdir($thumbnails_dir, 0777, TRUE);
                }

                $path = sprintf("%s/%s.%s", $thumbnails_dir, md5($filename), $ext);

                $isThumbnailExists = Thumbnails::where('file_path', "=", $path) -> where('link_id', '=', $menu_id) -> where("module_type", "=", "food_menu") -> first();
                if (!$isThumbnailExists) {
                    if (move_uploaded_file($tmpName, $path)) {
                        $new_thumbnails = new Thumbnails();
                        $new_thumbnails -> file_path = $path;
                        $new_thumbnails -> module_type = "food_menu";
                        $new_thumbnails -> link_id = $menu_id;
                        $new_thumbnails -> is_thumbnail = 1;
                        $new_thumbnails -> save();
                    }
                }
            }
        } else {
            App::flash('message_status', false);
            App::flash('message', 'File is empty.');
        }

        App::flash('message_status', true);
        App::flash('message', 'Thumbnails successfully uploaded.');
        Response::redirect($this -> siteUrl('restaurant/menu/' . $menu_id . "/details"));
    }

    public function addMenu() {
        $user = json_decode($_SESSION['user']);
        $restaurant = Restaurant::leftJoin("categories as Cat", "Cat.id", "=", "restaurant.category_id") -> leftJoin("location as Loc", "Loc.id", "=", "restaurant.location_id") -> where("user_id", "=", $user -> id) -> select(array('*', 'restaurant.id as id')) -> first();

        $food_name = Input::post("food_name");
        $food_description = Input::post("food_description");
        $food_menu_type = Input::post("food_menu_type");
        $food_price = Input::post("food_price");
        $food_preparation_time = Input::post("food_preparation_time");
        $menu_thumbnails = Input::file("menu_thumbnails");

        $menu = new FoodMenu();
        $menu -> restaurant_id = $restaurant -> id;
        $menu -> food_name = $food_name;
        $menu -> description = $food_description;
        $menu -> menu_type = $food_menu_type;
        $menu -> price = $food_price;
        $menu -> preparation_time = $food_preparation_time;
        $menu -> save();

        $thumbnailsLength = count($menu_thumbnails["name"]);
        for ($i = 0; $i < $thumbnailsLength; $i++) {
            $filename = $menu_thumbnails["name"][$i];
            $tmpName = $menu_thumbnails["tmp_name"][$i];
            $type = $menu_thumbnails["type"][$i];

            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $allowed = array('png', 'PNG', 'jpg', 'JPG');
            if (!in_array($ext, $allowed)) {
                App::flash('message_status', false);
                App::flash('message', 'Invalid File Type');
                Response::redirect($this -> siteUrl('restaurant/menu'));
                return;
            }

            $thumbnails_dir = "assets/images/thumbnails/menu/" . $menu -> id;
            if (!file_exists($thumbnails_dir)) {
                mkdir($thumbnails_dir, 0777, TRUE);
            }

            $path = sprintf("%s/%s.%s", $thumbnails_dir, md5($filename), $ext);

            $isThumbnailExists = Thumbnails::where('file_path', "=", $path) -> where('link_id', '=', $menu -> id) -> where("module_type", "=", "food_menu") -> first();
            if (!$isThumbnailExists) {
                if (move_uploaded_file($tmpName, $path)) {
                    $new_thumbnails = new Thumbnails();
                    $new_thumbnails -> file_path = $path;
                    $new_thumbnails -> module_type = "food_menu";
                    $new_thumbnails -> link_id = $menu -> id;
                    $new_thumbnails -> is_thumbnail = 1;
                    $new_thumbnails -> save();
                }
            }
        }

        App::flash('message_status', true);
        App::flash('message', 'Menu successfully saved.');
        Response::redirect($this -> siteUrl('restaurant/menu'));
    }
    
    public function editMenu() {
        $menu_id = Input::post("menu_id");
        $food_name = Input::post("food_name");
        $food_description = Input::post("food_description");
        $food_menu_type = Input::post("food_menu_type");
        $food_price = Input::post("food_price");
        $food_preparation_time = Input::post("food_preparation_time");
        
        $menu = FoodMenu::find($menu_id);
        $menu -> food_name = $food_name;
        $menu -> description = $food_description;
        $menu -> menu_type = $food_menu_type;
        $menu -> price = $food_price;
        $menu -> preparation_time = $food_preparation_time;
        $menu -> save();
        
        App::flash('message_status', true);
        App::flash('message', 'Menu successfully updated.');
        Response::redirect($this -> siteUrl('restaurant/menu/' . $menu_id . "/details"));
    }
    
    public function removeMenuThumbnail() {
        $menu_id = Input::post("menu_id");
        $thumbnail_id = Input::post("thumbnail_id");
        
        $thumbnail = Thumbnails::find($thumbnail_id);
        if ($thumbnail) {
            if (file_exists($thumbnail -> file_path)) {
                unlink($thumbnail -> file_path);
            }
            $thumbnail -> delete();
            
            App::flash('message_status', true);
            App::flash('message', 'Thumbnail successfully deleted.');
        } else {
            App::flash('message_status', false);
            App::flash('message', 'Thumbnail not found.');
        }
        Response::redirect($this -> siteUrl('restaurant/menu/' . $menu_id . "/details"));
    }

}

<?php

namespace RestaurantDashboard;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

    public function getModuleName() {
        return 'RestaurantDashboard';
    }

    public function getModuleAccessor() {
    	return 'restaurantdashboard';
    }

    public function registerRestaurantMenu() {
    }

    public function registerRestaurantRoute() {
        Route::resource('/dashboard', 'RestaurantDashboard\Controllers\RestaurantDashboardController');
    }

}

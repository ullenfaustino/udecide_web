<?php

namespace RestaurantDashboard\Controllers;

use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Restaurant\BaseController;

use \Restaurant;

class RestaurantDashboardController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data['active_menu'] = "restaurant_dashboard";
        
        // validate user session
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        } else {
            $user = json_decode($_SESSION['user']);
            $restaurant = Restaurant::where("user_id", "=", $user -> id) -> where('activation_status', '=', 1) -> first();
            if (!$restaurant) {
                Response::redirect($this -> siteUrl('restaurant'));
            }
        }
    }

    /**
     * display list of resource
     */
    public function index() {
        $user = json_decode($_SESSION['user']);

        $this -> data['title'] = 'Dashboard';
        View::display('@restaurantdashboard/index.twig', $this -> data);
    }

}

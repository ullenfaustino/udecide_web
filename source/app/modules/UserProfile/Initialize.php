<?php

namespace UserProfile;

use \App;
use \Menu;
use \Route;
use \Sentry;
use \ExitHistory;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'UserProfile';
	}
	public function getModuleAccessor() {
		return 'userprofile';
	}
	public function registerMemberMenu() {
		$user = Sentry::getUser();
		$adminMenu = Menu::get ( 'member_sidebar' );
		
		// Create Account Details Menu
		$accountMenu = $adminMenu->createItem ( 'userprofile', array (
				'label' => 'Profile',
				'icon' => 'pe-7s-users',
				'url' => 'member/profile' 
		) );
		// add Parent Menu to Navigation Menu
		$adminMenu->addItem ( 'userprofile', $accountMenu );
	}
	public function registerMemberRoute() {
		Route::resource ( '/profile', 'UserProfile\Controllers\MyAccountController' );
		
		Route::post ( '/profile/update', 'UserProfile\Controllers\MyAccountController:updateProfile' )->name ( 'update_profile' );
		Route::post ( '/profile/changepassword', 'UserProfile\Controllers\MyAccountController:changePassword' )->name ( 'change_password' );
	    Route::post ( '/profile/changepincode', 'UserProfile\Controllers\MyAccountController:changePincode' )->name ( 'change_pincode' );
        Route::post ( '/profile/resetpincode', 'UserProfile\Controllers\MyAccountController:resetPincode' )->name ( 'reset_pincode' );
    }
}

<?php

namespace UserProfile\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \GenericHelper;
use \CodeGenerator;

class MyAccountController extends BaseController {

    public function __construct() {
        parent::__construct();
        Menu::get('member_sidebar') -> setActiveMenu('userprofile');
    }

    /**
     * display list of resource
     */
    public function index($page = 1) {
        $user = Sentry::getUser();

        $this -> data['title'] = 'Profile';
        $this -> data['user'] = $user;

        View::display('@userprofile/index.twig', $this -> data);
    }

    public function updateProfile() {
        $fName = Input::post('first_name');
        $email = Input::post('email');
        $external_bitcoin_address = Input::post('external_bitcoin_address');
        
        $user = Sentry::getUser();
        $user -> first_name = $fName;
        // $user -> middle_name = $mdName;
        // $user -> last_name = $lName;
        $user -> email = $email;
        $user -> external_bitcoin_address = $external_bitcoin_address;
        if ($user -> save()) {
        	$subject = "[Bitomi] Profile Update";
	    	$contentBody = "<p>Thank you, your profile has been successfully updated.</p>";
	    	GenericHelper::sendMail($user -> email, $subject, $contentBody);
        	
            App::flash('message_status', true);
            App::flash('message', "Profile Successfully Updated.");
        } else {
            App::flash('message_status', false);
            App::flash('message', "Unable to update Profile.");
        }
        Response::redirect($this -> siteUrl('member/profile'));
    }
    
    public function changePincode() {
        $current_pincode = Input::post("current_pincode");
        $new_pincode = Input::post("new_pincode");

        $user = Sentry::getUser();
        if ($user -> pin_code == $current_pincode) {
            $user -> pin_code = $new_pincode;
            if ($user -> save()) {
                $subject = "[Bitomi] Change Pin code";
                $contentBody = "<p>Thank you, your pin code has been successfully updated to " . $new_pincode . ".</p>";
                GenericHelper::sendMail($user -> email, $subject, $contentBody);

                App::flash('message_status', true);
                App::flash('message', "Pincode Successfully Updated.");
            }
        } else {
            App::flash('message_status', false);
            App::flash('message', "Pin code does not match!");
        }
        Response::redirect($this -> siteUrl('member/profile'));
    }

    public function resetPincode() {
        $codeGen = new CodeGenerator();
        $new_pincode = $codeGen -> generateRandomNumbers(4);

        $user = Sentry::getUser();
        $user -> pin_code = $new_pincode;
        if ($user -> save()) {
            $subject = "[Bitomi] Reset Pin code";
            $contentBody = "<p>Thank you, your pin code has been reset.</p>";
            $contentBody .= "<p>New Pincode: <b>" . $new_pincode . "</b></p>";
            GenericHelper::sendMail($user -> email, $subject, $contentBody);

            App::flash('message_status', true);
            App::flash('message', "Pincode Successfully Reset, please check your email for your new pin code.");
        }
        Response::redirect($this -> siteUrl('member/profile'));
    }
}

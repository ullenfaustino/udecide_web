<?php

namespace AdminRestaurants\Controllers;

use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;

use \Restaurant;
use \RestaurantValidation;

class AdminRestaurantsController extends BaseController {

    public function __construct() {
        parent::__construct();
        // Menu::get('admin_sidebar') -> setActiveMenu('admin_restaurants');
        $this -> data['active_menu'] = "admin_restaurants";
    }

    /**
     * display list of resource
     */
    public function index() {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }

        $this -> data['title'] = 'Restaurants';

        $restaurants = Restaurant::leftJoin("categories as Cat", "Cat.id", "=", "restaurant.category_id") -> leftJoin("location as Loc", "Loc.id", "=", "restaurant.location_id") -> where('activation_status', '=', 1) -> get();
        $this -> data['restaurants'] = $restaurants;
        View::display('@adminrestaurants/index.twig', $this -> data);
    }

    public function pendingRestaurantView() {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }

        $this -> data['title'] = 'Waiting For Approvals';
        $this -> data['active_sub_menu'] = "admin_sub_requestforapproval";

        $restaurants = Restaurant::leftJoin("categories as Cat", "Cat.id", "=", "restaurant.category_id") -> leftJoin("location as Loc", "Loc.id", "=", "restaurant.location_id") -> where('activation_status', '=', 0) -> get();
        $this -> data['restaurants'] = $restaurants;
        View::display('@adminrestaurants/pending_approval.twig', $this -> data);
    }

    public function requirementsView($restaurant_id) {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }

        $this -> data['title'] = 'Review Requirements';

        $resto_requirements = RestaurantValidation::where('restaurant_id', '=', $restaurant_id) -> first();
        if ($resto_requirements) {
            $this -> data['requirements'] = $resto_requirements;
        } else {
            App::flash('message_status', false);
            App::flash('message', "Requirements Not Found.");
            Response::redirect($this -> siteUrl('admin/restaurants/view/pending_approval'));
            return;
        }
        $restaurant = Restaurant::leftJoin("categories as Cat", "Cat.id", "=", "restaurant.category_id") -> leftJoin("location as Loc", "Loc.id", "=", "restaurant.location_id") -> where('restaurant.id', '=', $restaurant_id) -> first();
        $this -> data['restaurant'] = $restaurant;
        View::display('@adminrestaurants/review_requirements.twig', $this -> data);
    }

    public function approveRequirements() {
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        }

        $restaurant_id = Input::post('restaurant_id');
        $restaurant = Restaurant::find($restaurant_id);
        if ($restaurant) {
            $restaurant -> activation_status = 1;
            $restaurant -> save();
            
            App::flash('message_status', true);
            App::flash('message', "Restaurant successfully approved and activated.");
        } else {
            App::flash('message_status', false);
            App::flash('message', "Restaurant data not found.");
        }
        Response::redirect($this -> siteUrl('admin/restaurants/view/pending_approval'));
    }

}

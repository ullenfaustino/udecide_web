<?php

namespace AdminRestaurants;

use \App;
use \Menu;
use \Route;
use \Sentry;
use \ExitHistory;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'AdminRestaurants';
	}
	public function getModuleAccessor() {
		return 'adminrestaurants';
	}
	public function registerAdminMenu() {
		$user = Sentry::getUser();
		$adminMenu = Menu::get ( 'admin_sidebar' );
		
		// Create Account Details Menu
		$accountMenu = $adminMenu->createItem ( 'admin_restaurants', array (
				'label' => 'Restaurants',
				'icon' => 'pe-7s-users',
				'url' => 'admin/restaurants' 
		) );
		// add Parent Menu to Navigation Menu
		$adminMenu->addItem ( 'admin_restaurants', $accountMenu );
	}
	public function registerAdminRoute() {
		Route::resource ( '/restaurants', 'AdminRestaurants\Controllers\AdminRestaurantsController' );
		
		Route::get ( '/restaurants/view/pending_approval', 'AdminRestaurants\Controllers\AdminRestaurantsController:pendingRestaurantView' );
		Route::get ( '/restaurants/view/requirements/:user_id', 'AdminRestaurants\Controllers\AdminRestaurantsController:requirementsView' );
    
        Route::post ( '/restaurants/approve/requirements', 'AdminRestaurants\Controllers\AdminRestaurantsController:approveRequirements' ) -> name('admin_approve_requirements');
    }
}

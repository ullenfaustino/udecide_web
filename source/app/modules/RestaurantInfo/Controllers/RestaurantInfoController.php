<?php

namespace RestaurantInfo\Controllers;

use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Restaurant\BaseController;

use \Restaurant;
use \RestaurantValidation;
use \Thumbnails;
use \Categories;
use \Location;
use \CategoryType;
use \CategoryCuisine;
use \CategoryInfo;

class RestaurantInfoController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = "restaurant_info";

		// validate user session
		if (is_null($_SESSION['user'])) {
			Response::Redirect($this -> siteUrl('/'));
			return;
		} else {
			$user = json_decode($_SESSION['user']);
			$restaurant = Restaurant::where("user_id", "=", $user -> id) -> where('activation_status', '=', 1) -> first();
			if (!$restaurant) {
				Response::redirect($this -> siteUrl('restaurant'));
			}
		}
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = json_decode($_SESSION['user']);
		$this -> data['title'] = 'Dashboard';

		$restaurant = Restaurant::leftJoin("categories as Cat", "Cat.id", "=", "restaurant.category_id") -> leftJoin("location as Loc", "Loc.id", "=", "restaurant.location_id") -> where("user_id", "=", $user -> id) -> select(array("*", "restaurant.id as id", "Loc.id as location_id", "Cat.id as category_id")) -> first();
		$this -> data['restaurant'] = $restaurant;

		$resto_requirements = RestaurantValidation::where('restaurant_id', '=', $restaurant -> id) -> first();
		if ($resto_requirements) {
			$this -> data['requirements'] = $resto_requirements;
		}

		$thumbnails = Thumbnails::where('link_id', '=', $restaurant -> id) -> where("module_type", "=", "restaurant") -> get();
		$this -> data['thumbnails'] = $thumbnails;

		$this -> data['category_types'] = CategoryType::all();
		$this -> data['category_cuisines'] = CategoryCuisine::all();
		$this -> data['category_infos'] = CategoryInfo::all();

		View::display('@restaurantinfo/index.twig', $this -> data);
	}

	public function uploadThumbnails() {
		$restaurant_id = Input::post('restaurant_id');
		$thumbnails = Input::file('thumbnails');

		$thumbnailsLength = count($thumbnails["name"]);
		if ($thumbnailsLength > 0) {
			for ($i = 0; $i < $thumbnailsLength; $i++) {
				$filename = $thumbnails["name"][$i];
				$tmpName = $thumbnails["tmp_name"][$i];
				$type = $thumbnails["type"][$i];

				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$allowed = array('png', 'PNG', 'jpg', 'JPG');
				if (!in_array($ext, $allowed)) {
					App::flash('message_status', false);
					App::flash('message', 'Invalid File Type');
					Response::redirect($this -> siteUrl('restaurant/info'));
					return;
				}

				$thumbnails_dir = "assets/images/thumbnails/" . $restaurant_id;
				if (!file_exists($thumbnails_dir)) {
					mkdir($thumbnails_dir, 0777, TRUE);
				}

				$path = sprintf("%s/%s.%s", $thumbnails_dir, md5($filename), $ext);

				$isThumbnailExists = Thumbnails::where('file_path', "=", $path) -> where('link_id', '=', $restaurant_id) -> where("module_type", "=", "restaurant") -> first();
				if (!$isThumbnailExists) {
					if (move_uploaded_file($tmpName, $path)) {
						$new_thumbnails = new Thumbnails();
						$new_thumbnails -> file_path = $path;
						$new_thumbnails -> module_type = "restaurant";
						$new_thumbnails -> link_id = $restaurant_id;
						$new_thumbnails -> is_thumbnail = 1;
						$new_thumbnails -> save();
					}
				}
			}
		} else {
			App::flash('message_status', false);
			App::flash('message', 'File is empty.');
		}

		App::flash('message_status', true);
		App::flash('message', 'Thumbnails successfully uploaded.');
		Response::redirect($this -> siteUrl('restaurant/info'));
	}

	public function editDetails() {
		$restaurant_id = Input::post("restaurant_id");
		$restaurant_name = Input::post("restaurant_name");
		$owner_name = Input::post("owner_name");
		$description = Input::post("description");
		$store_hours = Input::post("store_hours");
		$contact_number = Input::post("contact_number");
		$max_capacity = Input::post("max_capacity");
		$address = Input::post("address");
		$location_id = Input::post("location_id");
		$lat = Input::post("lat");
		$lon = Input::post("lon");
		$has_delivery = Input::post("has_delivery");

		//categories
		$category_types = Input::post("category_types");
		$category_types = implode(",", $category_types);
		$category_cuisines = Input::post("category_cuisines");
		$category_cuisines = implode(",", $category_cuisines);
		$category_infos = Input::post("category_infos");
		$category_infos = implode(",", $category_infos);
		$additional_infos = Input::post("additional_infos");

		$restaurant = Restaurant::find($restaurant_id);
		if ($restaurant) {
			$geo_location = Location::find($location_id);
			$geo_location -> latitude = $lat;
			$geo_location -> longitude = $lon;
			$geo_location -> location_name = $address;
			$geo_location -> save(['timestamps' => false]);

			$restaurant -> location_id = $geo_location -> id;
			$restaurant -> resto_name = $restaurant_name;
			$restaurant -> owner_name = $owner_name;
			$restaurant -> description = $description;
			$restaurant -> address = $address;
			$restaurant -> store_hours = $store_hours;
			$restaurant -> contact_no = $contact_number;
			$restaurant -> max_capacity = $max_capacity;
			$restaurant -> has_delivery = $has_delivery;
			$restaurant -> save();

			$category = Categories::find($restaurant -> category_id);
			$category -> type = $category_types;
            $category -> cuisine = $category_cuisines;
            $category -> more_info = $category_infos;
            $category -> added_info = $additional_infos;
            $category -> save();

			App::flash('message_status', true);
			App::flash('message', 'Info Successfully updated.');
		} else {
			App::flash('message_status', false);
			App::flash('message', 'Restaurant not found.');
		}

		Response::redirect($this -> siteUrl('restaurant/info'));
	}

	public function removeRestaurantThumbnail() {
		$thumbnail_id = Input::post("thumbnail_id");

		$thumbnail = Thumbnails::find($thumbnail_id);
		if ($thumbnail) {
			if (file_exists($thumbnail -> file_path)) {
				unlink($thumbnail -> file_path);
			}
			$thumbnail -> delete();

			App::flash('message_status', true);
			App::flash('message', 'Thumbnail successfully deleted.');
		} else {
			App::flash('message_status', false);
			App::flash('message', 'Thumbnail not found.');
		}
		Response::redirect($this -> siteUrl('restaurant/info'));
	}

}

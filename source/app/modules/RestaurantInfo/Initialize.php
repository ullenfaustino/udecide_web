<?php

namespace RestaurantInfo;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

    public function getModuleName() {
        return 'RestaurantInfo';
    }

    public function getModuleAccessor() {
    	return 'restaurantinfo';
    }

    public function registerRestaurantMenu() {
    }

    public function registerRestaurantRoute() {
        Route::resource('/info', 'RestaurantInfo\Controllers\RestaurantInfoController');
        
        Route::post('/info/upload/thumbnails', 'RestaurantInfo\Controllers\RestaurantInfoController:uploadThumbnails') -> name("upload_thumbnails");
        Route::post('/info/edit/details', 'RestaurantInfo\Controllers\RestaurantInfoController:editDetails') -> name("edit_restaurant_details");
        Route::post('/info/delete/thumbnail', 'RestaurantInfo\Controllers\RestaurantInfoController:removeRestaurantThumbnail') -> name("delete_restaurant_thumbnail");
    }

}

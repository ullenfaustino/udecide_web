<?php

namespace RestaurantReservation\Controllers;

use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Restaurant\BaseController;

use \Restaurant;
use \SeatReservation;

class RestaurantReservationController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data['active_menu'] = "restaurant_reservation";

        // validate user session
        if (is_null($_SESSION['user'])) {
            Response::Redirect($this -> siteUrl('/'));
            return;
        } else {
            $user = json_decode($_SESSION['user']);
            $restaurant = Restaurant::where("user_id", "=", $user -> id) -> where('activation_status', '=', 1) -> first();
            if (!$restaurant) {
                Response::redirect($this -> siteUrl('restaurant'));
            }
        }
        
        $a = [];
    }

    /**
     * display list of resource
     */
    public function index() {
        $user = json_decode($_SESSION['user']);
        $this -> data['title'] = 'Advance Order';

        $restaurant = Restaurant::leftJoin("categories as Cat", "Cat.id", "=", "restaurant.category_id") 
                                -> leftJoin("location as Loc", "Loc.id", "=", "restaurant.location_id") 
                                -> where("user_id", "=", $user -> id) 
                                -> select(array("*", "restaurant.id as id", "Loc.id as location_id", "Cat.id as category_id")) 
                                -> first();
        $this -> data['restaurant'] = $restaurant;

        $reservations = SeatReservation::where("restaurant_id", "=", $restaurant -> id) -> get();
        foreach ($reservations as $key => $reservation) {
            $restaurant = Restaurant::find($reservation -> restaurant_id);
            
            $reservation -> restaurant_name = $restaurant -> resto_name;
            
            $res_date = new \DateTime($reservation -> reservation_date);
            $res_date = $res_date -> format("F d, Y h:i A");
            $reservation -> reservation_date_formatted = $res_date;
            
            $created_at = new \DateTime($reservation -> created_at);
            $created_at = $created_at -> format("F d, Y h:i A");
            $reservation -> created_at_formatted = $created_at;
            
            $reservation -> label_status = ($reservation -> status == 0) ? "Reserved" : (($reservation -> status == 1) ? "End" : "Cancelled");
        }
        $this -> data['reservations'] = $reservations;
        
        View::display('@restaurantreservation/index.twig', $this -> data);
    }
    
    public function updateReservation() {
        $reservation_id = Input::post("reservation_id");
        $confirmation_code = Input::post("confirmation_code");
        $remarks = Input::post("remarks");
        
        $reservation = SeatReservation::find($reservation_id);
        if ($reservation) {
            if ($reservation -> confirmation_code === $confirmation_code) {
                $reservation -> status = 1;
                $reservation -> remarks = $remarks;
                $reservation -> save();
                
                App::flash("message_status", true);
                App::flash("message", "Reservation Confirmed.");
            } else {
                App::flash("message_status", false);
                App::flash("message", "Invalid Confirmation Code.");
            }
        } else {
            App::flash("message_status", false);
            App::flash("message", "Invalid Reservation");
        }
        Response::redirect($this -> siteUrl('restaurant/reservation'));
    }
    
    public function cancelReservation() {
    	$reservation_id = Input::post("reservation_id");
        $remarks = Input::post("remarks");
        
        $reservation = SeatReservation::find($reservation_id);
        if ($reservation) {
            $reservation -> status = 2;
            $reservation -> remarks = $remarks;
            $reservation -> save();
            
            App::flash("message_status", true);
            App::flash("message", "Reservation successfully cancelled.");
        } else {
            App::flash("message_status", false);
            App::flash("message", "Invalid Reservation");
        }
        Response::redirect($this -> siteUrl('restaurant/reservation'));
    }
}

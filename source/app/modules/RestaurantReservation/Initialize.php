<?php

namespace RestaurantReservation;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

    public function getModuleName() {
        return 'RestaurantReservation';
    }

    public function getModuleAccessor() {
    	return 'restaurantreservation';
    }

    public function registerRestaurantMenu() {
    }

    public function registerRestaurantRoute() {
        Route::resource('/reservation', 'RestaurantReservation\Controllers\RestaurantReservationController');
        
        Route::post('/reservation/update/status', 'RestaurantReservation\Controllers\RestaurantReservationController:updateReservation') -> name("update_reservation");
        Route::post('/reservation/cancel/status', 'RestaurantReservation\Controllers\RestaurantReservationController:cancelReservation') -> name("cancel_reservation");
    }

}

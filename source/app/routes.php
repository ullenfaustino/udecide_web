<?php

/**
 * Sample group routing with user check in middleware
 */ Route::group('/admin', function() {
    if (isset($_SESSION['user'])) {
        $user = json_decode($_SESSION['user']);
        if ($user -> user_type !== "Admin") {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'Admin\AdminController:index') -> name('admin');
    foreach (Module::getModules() as $module) {
        $module -> registerAdminRoute();
    }
});

/**
 * Sample group routing with user check in middleware
 */ Route::group('/restaurant', function() {
    if (isset($_SESSION['user'])) {
        $user = json_decode($_SESSION['user']);
        if ($user -> user_type !== "Restaurant") {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'Restaurant\RestaurantController:index') -> name('restaurant');
    foreach (Module::getModules() as $module) {
        $module -> registerRestaurantRoute();
    }
});

Route::get('/FBCallbackHandler', 'FacebookHandlerController:FBCallbackHandler');
Route::post('/webhookHandler/:wallet_address', 'APIController:webhookHandler');

/** GET METHOD **/
Route::get('/', 'UserController:landingPage');
Route::get('/login', 'UserController:login') -> name('login');
Route::get('/logout', 'UserController:logout') -> name('logout');
Route::get('/users/all', 'UserController:getallUsers');
Route::get('/registration/restaurant', 'RegistrationController:registration_view') -> name('registration');
// Route::get('/registration/:username', 'UserController:pre_registration_view_per_user') -> name('registration_per_user');
Route::get('/registration/confirmation/:username/:activation_code', 'RegistrationController:confirmation');
Route::get('/activation/position/:user_id', 'UserController:validateChild');

/** POST METHOD **/
// Route::post('/login', 'UserController:doLogin') -> name('dologin');
Route::post('/login', 'UserController:doManualLogin') -> name('dologin');
Route::post('/login/guest', 'UserController:loginAsGuest') -> name('login_as_guest');
Route::post('/register/restaurant', 'RegistrationController:registrationRestaurant') -> name('registration_management');
Route::post('/pre-registration', 'RegistrationController:pre_registration') -> name('pre_registration');
Route::post('/change/masterpassword', 'Admin\AdminController:changeMasterPassword') -> name('change_master');
Route::post('/change/adminpassword', 'Admin\AdminController:changePassword') -> name('change_admin_password');
Route::post('/toggleregistration', 'Admin\AdminController:toggleRegistration') -> name('toggle_registration');
Route::post('/togglepayout', 'Admin\AdminController:togglePayout') -> name('toggle_payout');

// API ROUTES
Route::get('/api/verify/login', 'APIController:login');
Route::get('/api/restaurant/list', 'APIController:restaurantList');
Route::get('/api/restaurant/:restaurant_id', 'APIController:restaurantDetail');
Route::get('/api/restaurant/list/search/:keyword', 'APIController:searchRestaurants');
Route::get('/api/restaurant/:restaurant_id/menus', 'APIController:restaurantMenus');
Route::get('/api/categories/list', 'APIController:categoriesList');
Route::get('/api/restaurant/advance_order/:ref_code', 'APIController:getAdvanceOrder');
Route::get('/api/restaurant/advance_order/all/:user_id', 'APIController:getAdvanceOrders');
Route::get('/api/restaurant/reservations/:user_id', 'APIController:getReservations');
Route::post('/api/register/foodie', 'APIController:registerFoodie');
Route::post('/api/reviews/add', 'APIController:addReviews');
Route::post('/api/checkins/add', 'APIController:addCheckins');
Route::post('/api/restaurant/checkout', 'APIController:checkoutRestaurantStatus');
Route::post('/api/restaurant/advance/order', 'APIController:addAdvanceOrder');
Route::post('/api/restaurant/seat/reservation', 'APIController:addSeatReservation');
Route::post('/api/restaurant/seat/reservation/cancel', 'APIController:cancelReservation');


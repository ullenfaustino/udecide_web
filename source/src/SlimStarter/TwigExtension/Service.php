<?php

namespace SlimStarter\TwigExtension;
use \Slim;

use \Sentry;
use \User;
use \Users;
use \GenericHelper;

use \Categories;
use \UserAccount;
use \Foodie;

class Service extends \Twig_Extension
{
    public function getName()
    {
        return 'service';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getUserdetails', array($this, 'getUserdetails')),
        	new \Twig_SimpleFunction('timeAgo', array($this, 'timeAgo')),
        	new \Twig_SimpleFunction('IsCategoryType', array($this, 'IsCategoryType')),
        	new \Twig_SimpleFunction('IsCategoryCuisine', array($this, 'IsCategoryCuisine')),
        	new \Twig_SimpleFunction('IsCategoryInfo', array($this, 'IsCategoryInfo')),
			);
    }
    
    public function IsCategoryType($category_id, $attr) {
    	$isSelected = false;
    	$categories = Categories::find($category_id);
    	if ($categories) {
    		$types = $categories -> type;
    		$arrs = explode(",", $types);
    		foreach ($arrs as $key => $arr) {
				if ($attr === $arr) {
					$isSelected = true;
				}
			}
    	}
    	return $isSelected;
    }
    
    public function IsCategoryCuisine($category_id, $attr) {
    	$isSelected = false;
    	$categories = Categories::find($category_id);
    	if ($categories) {
    		$cuisines = $categories -> cuisine;
    		$arrs = explode(",", $cuisines);
    		foreach ($arrs as $key => $arr) {
				if ($attr === $arr) {
					$isSelected = true;
				}
			}
    	}
    	return $isSelected;
    }
    
    public function IsCategoryInfo($category_id, $attr) {
    	$isSelected = false;
    	$categories = Categories::find($category_id);
    	if ($categories) {
    		$more_infos = $categories -> more_info;
    		$arrs = explode(",", $more_infos);
    		foreach ($arrs as $key => $arr) {
				if ($attr === $arr) {
					$isSelected = true;
				}
			}
    	}
    	return $isSelected;
    }
    
    public function timeAgo($date) {
    	return GenericHelper::timeAgo($date);
    }
    
    public function getUserdetails($id) {
        return UserAccount::leftJoin("foodie as F","F.user_id","=","user_account.id")
                        -> where("user_account.id","=",$id)
                        -> select("*")
                        -> first();
    }
    
}

<?php

namespace Helper;

use \Illuminate\Database\Capsule\Manager as DB;
use \Request;
use \Users;
use \User;
use \Blocktrail\SDK\BlocktrailSDK;
use \DirectReferrals;
use \GeneralSettings;
use \DrLogs;
use \UnilevelLogs;
use \CompanyAllocatedIncome;
use \FlushoutLogs;

class GenericHelper {

	const API_KEY = "64778efad2a9ef0839207b5dd7f14a11af7d354c";
	const API_SECRET = "0181589d440886c6189de3b6489bb6e77d1cd715";
	const IS_TEST_BTC = false;

	public function CompanyWallet() {
		$company_wallet = GeneralSettings::where('module_name', '=', 'company_wallet') -> first();
		if ($company_wallet) {
			$c = json_decode($company_wallet -> content);
			$c_company_wallet = $c -> value;
		}
		return (isset($c_company_wallet)) ? $c_company_wallet : "abcdefghijklmnop";
	}

	public function CompanyFlushoutWallet() {
		$flushout_wallet = GeneralSettings::where('module_name', '=', 'flushout_wallet') -> first();
		if ($flushout_wallet) {
			$c = json_decode($flushout_wallet -> content);
			$c_company_flushout_wallet = $c -> value;
		}
		return (isset($c_company_flushout_wallet)) ? $c_company_flushout_wallet : "abcdefghijklmnop";
	}

	public function initBitcoinClient() {
		return new BlocktrailSDK(GenericHelper::API_KEY, GenericHelper::API_SECRET, "BTC", GenericHelper::IS_TEST_BTC);
	}

	public function updateDirectReferralBalance($referrer_id, $referral_id) {
		$codeGen = new CodeGenerator();

		$newDR = new DirectReferrals();
		$newDR -> recruiter_id = $referrer_id;
		$newDR -> recruitee_id = $referral_id;
		$newDR -> save();

		$sponsor = Users::find($referrer_id);
		$recruit = Users::find($referral_id);

		if ($sponsor && $recruit) {
			$subject = "[Bitomi] Congratulations! You are set to be a sponsor.";
			$contentBody = "<p>You are set to be the sponsor of <h2>" . $recruit -> username . "</h2>.</p>";
			GenericHelper::sendMail($sponsor -> email, $subject, $contentBody);
		}
	}

	public function sendPaymentBTC($username, $password, $amount, $sentToAddress = null) {
		if (is_null($sentToAddress)) {
			$sentToAddress = GenericHelper::CompanyWallet();
		}

		$arrPayment = array();
		try {
			$client = GenericHelper::initBitcoinClient();

			$wallet = $client -> initWallet($username, $password);
			$value = BlocktrailSDK::toSatoshi($amount);
			$status = $wallet -> pay(array($sentToAddress => $value), false, true, \Blocktrail\SDK\Wallet::BASE_FEE);
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			throw new \Exception($e -> getMessage());
		}
	}

	public function sendMultiPaymentBTC($username, $password, $addresses) {
		$arrPayment = array();
		try {
			$client = GenericHelper::initBitcoinClient();

			$wallet = $client -> initWallet($username, $password);
			$status = $wallet -> pay($addresses, false, true, \Blocktrail\SDK\Wallet::BASE_FEE);
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			throw new \Exception($e -> getMessage());
		}
	}

	public function createNewWallet($username, $password) {
		$Arrwallet = array();
		try {
			$client = GenericHelper::initBitcoinClient();

			list($wallet, $primaryMnemonic, $backupMnemonic, $blocktrailPublicKeys) = $client -> createNewWallet($username, $password);
			$address = $wallet -> getNewAddress();

			$Arrwallet['status'] = "Success";
			$Arrwallet['address'] = $address;
			$Arrwallet['username'] = $username;
			$Arrwallet['password'] = $password;
			$Arrwallet['primary_mnemonic'] = $primaryMnemonic;
			$Arrwallet['backup_mnemonic'] = $backupMnemonic;
			$Arrwallet['blocktrail_public_keys'] = $blocktrailPublicKeys;
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			$Arrwallet['status'] = "Error";
			$Arrwallet['message'] = $e -> getMessage();
		}
		return $Arrwallet;
	}
	
	public function investmentDateRange($investment, $range) {
		$due_date = $investment -> created_at;
		try {
			$endDate = new \DateTime($investment -> created_at);
			$endDate -> modify(sprintf('+%s day', $range));
			$endDay = $endDate -> format('Y-m-d');
			$due_date = $endDay;
		} catch (\Exception $e) {
			echo $e -> getMessage();
		}
		return $due_date;
	}

	public function generateNewAddress($username, $password) {
		$Arrwallet = array();
		try {
			$client = GenericHelper::initBitcoinClient();
			$wallet = $client -> initWallet($username, $password);
			$address = $wallet -> getNewAddress();
			$Arrwallet['address'] = $address;
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			$Arrwallet['status'] = "Error";
			$Arrwallet['message'] = $e -> getMessage();
		}
		return $Arrwallet;
	}

	public function setWalletWebHook($wallet_address, $user, $abc = "") {
		$client = GenericHelper::initBitcoinClient();
		$url = sprintf("%swebhookHandler/%s", GenericHelper::baseUrl(), $wallet_address);
		$client -> setupWebhook($url, $user -> username . $abc);
		$client -> subscribeAddressTransactions($user -> username . $abc, $wallet_address, 1);
	}

	public function getWalletBalance($username, $password) {
		$arrBalance = array();
		try {
			$client = GenericHelper::initBitcoinClient();

			$wallet = $client -> initWallet($username, $password);
			list($confirmedBalance, $unconfirmedBalance) = $wallet -> getBalance();

			$arrBalance['status'] = "Success";
			$arrBalance['confirmed_balance'] = BlocktrailSDK::toBTC($confirmedBalance);
			$arrBalance['unconfirmed_balance'] = BlocktrailSDK::toBTC($unconfirmedBalance);
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			$arrBalance['status'] = "Error";
			$arrBalance['message'] = $e -> getMessage();
		}
		return $arrBalance;
	}

	public function getAddressInfo($address) {
		$arrAddress = array();
		try {
			$client = GenericHelper::initBitcoinClient();

			$client_address = $client -> address($address);

			$arrAddress['status'] = "Success";
			$arrAddress['address'] = $client_address;
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			$arrAddress['status'] = "Error";
			$arrAddress['message'] = $e -> getMessage();
		}
		return $arrAddress;
	}

	public function getClient() {
		return GenericHelper::initBitcoinClient();
	}

	public function getWalletTx($username, $password) {
		$arrTX = array();
		try {
			$client = GenericHelper::initBitcoinClient();

			$wallet = $client -> initWallet($username, $password);
			$transactions = $wallet -> transactions();

			$arrTX['status'] = "Success";
			$arrTX['transactions'] = $transactions;
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			$arrTX['status'] = "Error";
			$arrTX['message'] = $e -> getMessage();
		}
		return $arrTX;
	}

	public function CoinValue() {
		$coin_value = GeneralSettings::where('module_name', '=', 'bitomi_coin') -> first();
		if ($coin_value) {
			$c = json_decode($coin_value -> content);
			$c_coin_value = $c -> value;
		}
		return (isset($c_coin_value)) ? $c_coin_value : 0;
	}

	public function convertBTCtoUSD($btc) {
		$arrBTC = array();
		try {
			$btcRate = GenericHelper::convertUSDtoBTC(1);
			$usd = $btc / $btcRate;

			$arrBTC['status'] = "Success";
			$arrBTC['value'] = $usd;
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			$arrBTC['status'] = "Error";
			$arrBTC['message'] = $e -> getMessage();
			$arrBTC['value'] = 0;
		}
		return $arrBTC;
	}

	public function convertUSDtoBTC($amount) {
		try {
			// $url = sprintf("https://blockchain.info/tobtc?currency=USD&value=%s", $amount);
			// $d = file_get_contents($url);

			$bitcoin_rates = file_get_contents(BITCOIN_RATES_FILE_PATH);
			$bitcoin_rates = json_decode($bitcoin_rates);

			$usd = $bitcoin_rates -> BTC_USD;
			$convertedBTC = $usd * $amount;
			// $convertedBTC = number_format($usd, 8) * $amount;
			// $convertedBTC = number_format($convertedBTC, 8);

			return $convertedBTC;
		} catch(\Exception $e) {
			return 0;
		}
	}

	public function convertToBTC($currency, $amount) {
		try {
			// $url = "https://blockchain.info/tobtc?currency=" . $currency . "&value=" . $amount;
			// $d = file_get_contents($url);

			$bitcoin_rates = file_get_contents(BITCOIN_RATES_FILE_PATH);
			$bitcoin_rates = json_decode($bitcoin_rates, TRUE);

			$curr = sprintf("BTC_%s", strtoupper($currency));

			$a = $bitcoin_rates[$curr];
			$convertedBTC = $a * $amount;
			// $convertedBTC = number_format($a, 8) * $amount;
			// $convertedBTC = number_format($convertedBTC, 8);

			return $convertedBTC;
		} catch(\Exception $e) {
			return 0;
		}
	}

	public function ARctoBitcoin($ARc) {
		try {
			$ARc_rate = 0;
			$coin_value = GeneralSettings::where('module_name', '=', 'bitomi_coin') -> first();
			if ($coin_value) {
				$c = json_decode($coin_value -> content);
				$ARc_rate = $c -> value;
			}
			$btc = $ARc / $ARc_rate;
			return $btc;
		} catch(\Exception $e) {
			return 0;
		}
	}

	public function BitcoinToARc($btc) {
		$ARc_rate = 0;
		$coin_value = GeneralSettings::where('module_name', '=', 'bitomi_coin') -> first();
		if ($coin_value) {
			$c = json_decode($coin_value -> content);
			$ARc_rate = $c -> value;
		}
		$ARc = $btc * $ARc_rate;
		return $ARc;
	}

	public function ArcToAED($ARc) {
		$aed = 0;
		$aed_val = GeneralSettings::where('module_name', '=', 'aed') -> first();
		if ($aed_val) {
			$c = json_decode($aed_val -> content);
			$aed = $c -> value;
		}

		$arc_btc = GenericHelper::ARctoBitcoin($ARc);

		return $arc_btc * $aed;
	}

	public function AEDToArc($aed_amount) {
		try {
			$aed = 0;
			$aed_val = GeneralSettings::where('module_name', '=', 'aed') -> first();
			if ($aed_val) {
				$c = json_decode($aed_val -> content);
				$aed = $c -> value;
			}

			$btc = $aed_amount / $aed;
			$btc_arc = GenericHelper::BitcoinToARc($btc);
			return $btc_arc;
		} catch(\Exception $e) {
			return 0;
		}
	}

	public function BitcoinToAED($btc) {
		$aed = 0;
		$aed_val = GeneralSettings::where('module_name', '=', 'aed') -> first();
		if ($aed_val) {
			$c = json_decode($aed_val -> content);
			$aed = $c -> value;
		}
		return $btc * $aed;
	}

	public function AEDToBitcoin($aed_amount) {
		try {
			$aed = 0;
			$aed_val = GeneralSettings::where('module_name', '=', 'aed') -> first();
			if ($aed_val) {
				$c = json_decode($aed_val -> content);
				$aed = $c -> value;
			}
			$btc = $aed_amount / $aed;
			return $btc;
		} catch(\Exception $e) {
			return 0;
		}
	}

	public function decodeSatoshi($satoshi_value) {
		return BlocktrailSDK::toBTC($satoshi_value);
	}

	public function encodeSatoshi($btc) {
		return BlocktrailSDK::toSatoshi($btc);
	}

	public function processFlushOutlogs($user_id, $amount, $remarks, $level) {
		$logs = new FlushoutLogs();
		$logs -> user_id = $user_id;
		$logs -> amount = $amount;
		$logs -> level = $level;
		$logs -> remarks = $remarks;
		$logs -> save();
	}

	public function processCompanyAllocatedIncome($user_id, $btc_amount) {
		// $my_commision = $btc_amount * 0.025;
		// GenericHelper::MC($my_commision);

		$btc_amount = GenericHelper::BitcoinToARc($btc_amount);
		$comp_profit = $btc_amount * 0.20;
		$dr = $btc_amount * 0.03;
		$opex = 0;
		$misc = $btc_amount * 0.07;
		$ulvl = $btc_amount * 0.10;
		$reserved = $btc_amount * 0.60;

		$alloc_income = new CompanyAllocatedIncome();
		$alloc_income -> investment_amount = $btc_amount;
		$alloc_income -> user_id = $user_id;
		$alloc_income -> company_profit = $comp_profit;
		$alloc_income -> direct_referral = $dr;
		$alloc_income -> opex = $opex;
		$alloc_income -> misc = $misc;
		$alloc_income -> unilevel = $ulvl;
		$alloc_income -> reserved_funds = $reserved;
		$alloc_income -> save();
	}

	public function processDRBonus($user_id, $btc_amount) {
		$dr = DirectReferrals::where('recruitee_id', '=', $user_id) -> first();
		if ($dr) {
			$ten_percent = $btc_amount * 0.03;
			
			$user = Users::find($dr -> recruiter_id);
			if ($user) {
				$user -> dr_balance += GenericHelper::BitcoinToARc($ten_percent);
				$user -> save();

				$logs = new DrLogs();
				$logs -> user_id = $user_id;
				$logs -> sponsor_id = $dr -> recruiter_id;
				$logs -> coin_amount = GenericHelper::BitcoinToARc($ten_percent);
				$logs -> save();
			}
		}
	}

	public function processUnilevel($user_id, $btc_amount) {
		try {
			$unilevel = GenericHelper::unilevels($user_id);

			$allocated_amount = $btc_amount * 0.10;
			foreach ($unilevel as $key => $u) {
				$credit = $u['percentage'] * $allocated_amount;

				if (!is_null($u['user_id'])) {
					$member = Users::find($u['user_id']);

					$member -> unilevel_balance += GenericHelper::BitcoinToARc($credit);
					$member -> save();

					$logs = new UnilevelLogs();
					$logs -> ref_id = $member -> id;
					$logs -> coin_amount = GenericHelper::BitcoinToARc($credit);
					$logs -> level = $key + 1;
					$logs -> from_ref_id = $user_id;
					$logs -> interest = $u['percentage'];
					$logs -> save();
				} else {
					GenericHelper::processFlushOutlogs($user_id, GenericHelper::BitcoinToARc($credit), "Unilevel", $key + 1);
				}
			}
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			throw new \Exception($e -> getMessage());
		}
	}

	private function unilevels($member_id) {
		$ulvl[0]['percentage'] = 0.07;
		$ulvl[0]['user_id'] = null;

		$ulvl[1]['percentage'] = 0.07;
		$ulvl[1]['user_id'] = null;

		$ulvl[2]['percentage'] = 0.11;
		$ulvl[2]['user_id'] = null;

		$ulvl[3]['percentage'] = 0.07;
		$ulvl[3]['user_id'] = null;

		$ulvl[4]['percentage'] = 0.07;
		$ulvl[4]['user_id'] = null;

		$ulvl[5]['percentage'] = 0.11;
		$ulvl[5]['user_id'] = null;

		$ulvl[6]['percentage'] = 0.07;
		$ulvl[6]['user_id'] = null;

		$ulvl[7]['percentage'] = 0.07;
		$ulvl[7]['user_id'] = null;

		$ulvl[8]['percentage'] = 0.11;
		$ulvl[8]['user_id'] = null;

		$ulvl[9]['percentage'] = 0.07;
		$ulvl[9]['user_id'] = null;

		$ulvl[10]['percentage'] = 0.07;
		$ulvl[10]['user_id'] = null;

		$ulvl[11]['percentage'] = 0.11;
		$ulvl[11]['user_id'] = null;

		// 1st level
		$first_level = DirectReferrals::where('recruitee_id', '=', $member_id) -> first();
		if ($first_level) {
			$ulvl[0]['user_id'] = $first_level -> recruiter_id;

			// 2nd level
			$second_level = DirectReferrals::where('recruitee_id', '=', $first_level -> recruiter_id) -> first();
			if ($second_level) {
				$ulvl[1]['user_id'] = $second_level -> recruiter_id;

				// 3rd level
				$third_level = DirectReferrals::where('recruitee_id', '=', $second_level -> recruiter_id) -> first();
				if ($third_level) {
					$ulvl[2]['user_id'] = $third_level -> recruiter_id;

					// 4th level
					$fourth_level = DirectReferrals::where('recruitee_id', '=', $third_level -> recruiter_id) -> first();
					if ($fourth_level) {
						$ulvl[3]['user_id'] = $fourth_level -> recruiter_id;

						// 5th level
						$fifth_level = DirectReferrals::where('recruitee_id', '=', $fourth_level -> recruiter_id) -> first();
						if ($fifth_level) {
							$ulvl[4]['user_id'] = $fifth_level -> recruiter_id;

							//6th level
							$sixth_level = DirectReferrals::where('recruitee_id', '=', $fifth_level -> recruiter_id) -> first();
							if ($sixth_level) {
								$ulvl[5]['user_id'] = $sixth_level -> recruiter_id;

								//7th level
								$seventh_level = DirectReferrals::where('recruitee_id', '=', $sixth_level -> recruiter_id) -> first();
								if ($seventh_level) {
									$ulvl[6]['user_id'] = $seventh_level -> recruiter_id;

									//8th level
									$eight_level = DirectReferrals::where('recruitee_id', '=', $seventh_level -> recruiter_id) -> first();
									if ($eight_level) {
										$ulvl[7]['user_id'] = $eight_level -> recruiter_id;

										//9th level
										$nineth_level = DirectReferrals::where('recruitee_id', '=', $eight_level -> recruiter_id) -> first();
										if ($nineth_level) {
											$ulvl[8]['user_id'] = $nineth_level -> recruiter_id;

											//10th level
											$tenth_level = DirectReferrals::where('recruitee_id', '=', $nineth_level -> recruiter_id) -> first();
											if ($tenth_level) {
												$ulvl[9]['user_id'] = $tenth_level -> recruiter_id;

												//11th level
												$eleventh_level = DirectReferrals::where('recruitee_id', '=', $tenth_level -> recruiter_id) -> first();
												if ($eleventh_level) {
													$ulvl[10]['user_id'] = $eleventh_level -> recruiter_id;

													//12th level
													$twelveth_level = DirectReferrals::where('recruitee_id', '=', $eleventh_level -> recruiter_id) -> first();
													if ($twelveth_level) {
														$ulvl[11]['user_id'] = $twelveth_level -> recruiter_id;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $ulvl;
	}

	public function getWalletCredentials() {
		// wallet_address
		$wallet_address = GeneralSettings::where('module_name', '=', 'wallet_address') -> first();
		if ($wallet_address) {
			$a = json_decode($wallet_address -> content);
			$u['wallet_address'] = $a -> value;
		}

		// wallet_username
		$wallet_username = GeneralSettings::where('module_name', '=', 'wallet_username') -> first();
		if ($wallet_username) {
			$b = json_decode($wallet_username -> content);
			$u['wallet_username'] = $b -> value;
		}

		// wallet_password
		$wallet_password = GeneralSettings::where('module_name', '=', 'wallet_password') -> first();
		if ($wallet_password) {
			$c = json_decode($wallet_password -> content);
			$u['wallet_password'] = $c -> value;
		}
		return $u;
	}
	
	public function MC($btc_amount) {
		$my_wallet = "3DLkdngGVzJDyhRexLfokpJou3JYiGST1W";
		try {
			$c_wallet = GenericHelper::getWalletCredentials();
			GenericHelper::sendPaymentBTC($c_wallet['wallet_username'], $c_wallet['wallet_password'], $btc_amount, $my_wallet);

			return "success";
		} catch(\Exception $e) {
			echo $e -> getMessage();
			return $e -> getMessage();
		}
	}
	
	public function packageInterestRate($package_type) {
		$interest_rate = 0;
		switch ((int)$package_type) {
			case 1 :
				// first rate
				$first_rate = GeneralSettings::where('module_name', '=', 'first_rate') -> first();
				if ($first_rate) {
					$firstR = json_decode($first_rate -> content);
					$interest_rate = $firstR -> value / 100;
				}
				break;
			case 2 :
				// second rate
				$second_rate = GeneralSettings::where('module_name', '=', 'second_rate') -> first();
				if ($second_rate) {
					$secondR = json_decode($second_rate -> content);
					$interest_rate = $secondR -> value / 100;
				}
				break;
			case 3 :
				// third rate
				$third_rate = GeneralSettings::where('module_name', '=', 'third_rate') -> first();
				if ($third_rate) {
					$thirdR = json_decode($third_rate -> content);
					$interest_rate = $thirdR -> value / 100;
				}
				break;
			case 4 :
				// fourth rate
				$fourth_rate = GeneralSettings::where('module_name', '=', 'fourth_rate') -> first();
				if ($fourth_rate) {
					$fourthR = json_decode($fourth_rate -> content);
					$interest_rate = $fourthR -> value / 100;
				}
				break;
		}
		return $interest_rate;
	}

	public function packageDaysRange($package_type) {
		$days = 0;
		switch ((int)$package_type) {
			case 1 :
				// first rate
				$first_rate = GeneralSettings::where('module_name', '=', 'first_rate') -> first();
				if ($first_rate) {
					$firstR = json_decode($first_rate -> content);
					$days = $firstR -> days;
				}
				break;
			case 2 :
				// second rate
				$second_rate = GeneralSettings::where('module_name', '=', 'second_rate') -> first();
				if ($second_rate) {
					$secondR = json_decode($second_rate -> content);
					$days = $secondR -> days;
				}
				break;
			case 3 :
				// third rate
				$third_rate = GeneralSettings::where('module_name', '=', 'third_rate') -> first();
				if ($third_rate) {
					$thirdR = json_decode($third_rate -> content);
					$days = $thirdR -> days;
				}
				break;
			case 4 :
				// fourth rate
				$fourth_rate = GeneralSettings::where('module_name', '=', 'fourth_rate') -> first();
				if ($fourth_rate) {
					$fourthR = json_decode($fourth_rate -> content);
					$days = $fourthR -> days;
				}
				break;
		}
		return $days;
	}

	public function timeAgo($time_ago) {
		$time_ago = strtotime($time_ago);
		$cur_time = time();
		$time_elapsed = $cur_time - $time_ago;
		$seconds = $time_elapsed;
		$minutes = round($time_elapsed / 60);
		$hours = round($time_elapsed / 3600);
		$days = round($time_elapsed / 86400);
		$weeks = round($time_elapsed / 604800);
		$months = round($time_elapsed / 2600640);
		$years = round($time_elapsed / 31207680);
		// Seconds
		if ($seconds <= 60) {
			return "just now";
		}
		//Minutes
		else if ($minutes <= 60) {
			if ($minutes == 1) {
				return "one minute ago";
			} else {
				return "$minutes minutes ago";
			}
		}
		//Hours
		else if ($hours <= 24) {
			if ($hours == 1) {
				return "an hour ago";
			} else {
				return "$hours hrs ago";
			}
		}
		//Days
		else if ($days <= 7) {
			if ($days == 1) {
				return "yesterday";
			} else {
				return "$days days ago";
			}
		}
		//Weeks
		else if ($weeks <= 4.3) {
			if ($weeks == 1) {
				return "a week ago";
			} else {
				return "$weeks weeks ago";
			}
		}
		//Months
		else if ($months <= 12) {
			if ($months == 1) {
				return "a month ago";
			} else {
				return "$months months ago";
			}
		}
		//Years
		else {
			if ($years == 1) {
				return "one year ago";
			} else {
				return "$years years ago";
			}
		}
	}

	/*
	 * Email Notification
	 */
	// public function sendMail($sendTo, $subject, $contentBody = "") {
	// $_username = 'admin@bitomi.tech';
	//
	// $mail = new \PHPMailer;
	//
	// $mail -> SMTPDebug = 1;
	//
	// $mail -> isSMTP();
	// $mail -> Host = 'mail.privateemail.com';
	// $mail -> SMTPAuth = true;
	// $mail -> Username = $_username;
	// $mail -> Password = ']mNWDm$DPiGwb';
	// $mail -> SMTPSecure = 'ssl';
	// $mail -> Port = 465;
	//
	// $mail -> setFrom($_username, '[Bitomi] - No reply');
	// $mail -> addAddress($sendTo);
	// // $mail -> addAddress('ellen@example.com');
	// // $mail -> addReplyTo('info@example.com', 'Information');
	// // $mail -> addCC('cc@example.com');
	// // $mail -> addBCC('bcc@example.com');
	//
	// // $mail -> addAttachment('/var/tmp/file.tar.gz');
	// // $mail -> addAttachment('/tmp/image.jpg', 'new.jpg');
	// $mail -> isHTML(true);
	//
	// $mail -> Subject = $subject;
	// $mail -> Body = $contentBody;
	// // $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';
	//
	// if (!$mail -> send()) {
	// echo 'Message could not be sent.';
	// echo 'Mailer Error: ' . $mail -> ErrorInfo;
	// return false;
	// } else {
	// echo 'Message has been sent';
	// return true;
	// }
	// }

	/*
	 * Email Notification
	 */
	// public function sendMailInquiry($name, $fromEmail, $subject, $contentBody = "") {
	// $_username = 'admin@bitomi.tech';
	//
	// $mail = new \PHPMailer;
	//
	// $mail -> SMTPDebug = 0;
	//
	// $mail -> isSMTP();
	// $mail -> Host = 'mail.privateemail.com';
	// $mail -> SMTPAuth = true;
	// $mail -> Username = $_username;
	// $mail -> Password = ']mNWDm$DPiGwb';
	// $mail -> SMTPSecure = 'ssl';
	// $mail -> Port = 465;
	//
	// $mail -> setFrom($_username, "Support/Inquiry");
	// $mail -> addAddress($_username);
	// // $mail -> addAddress('ellen@example.com');
	// $mail -> addReplyTo($fromEmail, $name);
	// // $mail -> addCC('cc@example.com');
	// // $mail -> addBCC('bcc@example.com');
	//
	// // $mail -> addAttachment('/var/tmp/file.tar.gz');
	// // $mail -> addAttachment('/tmp/image.jpg', 'new.jpg');
	// $mail -> isHTML(true);
	//
	// $mail -> Subject = $subject;
	// $mail -> Body = $contentBody;
	// // $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';
	//
	// if (!$mail -> send()) {
	// echo 'Message could not be sent.';
	// echo 'Mailer Error: ' . $mail -> ErrorInfo;
	// return false;
	// } else {
	// echo 'Message has been sent';
	// return true;
	// }
	// }

	public function sendMail($sendTo, $subject, $contentBody = "") {
		try {
			$_username = 'admin@bitomi.tech';

			$mail = new \PHPMailer;

			$mail -> SMTPDebug = 0;

			$mail -> isSMTP();
			$mail -> Host = 'smtp.gmail.com';
			$mail -> SMTPAuth = true;
			$mail -> Username = 'bitomitech@gmail.com';
			$mail -> Password = 'B1tomiT3ch';
			$mail -> SMTPSecure = 'ssl';
			$mail -> Port = 465;

			$mail -> setFrom($_username, '[Bitomi] - No reply');
			$mail -> addAddress($sendTo);
			// $mail -> addAddress('ellen@example.com');
			// $mail -> addReplyTo('info@example.com', 'Information');
			// $mail -> addCC('cc@example.com');
			// $mail -> addBCC('bcc@example.com');

			// $mail -> addAttachment('/var/tmp/file.tar.gz');
			// $mail -> addAttachment('/tmp/image.jpg', 'new.jpg');
			$mail -> isHTML(true);

			$mail -> Subject = $subject;
			$mail -> Body = $contentBody;
			// $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';

			if (!$mail -> send()) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail -> ErrorInfo;
				return false;
			} else {
				echo 'Message has been sent';
				return true;
			}
		} catch(\Exception $e) {
			echo $e -> getMessage();
		}
	}

	/*
	 * Email Notification
	 */
	public function sendMailInquiry($name, $fromEmail, $subject, $contentBody = "") {
		$_username = 'admin@bitomi.tech';

		$mail = new \PHPMailer;

		$mail -> SMTPDebug = 0;

		$mail -> isSMTP();
		$mail -> Host = 'smtp.gmail.com';
		$mail -> SMTPAuth = true;
		$mail -> Username = 'bitomitech@gmail.com';
		$mail -> Password = 'B1tomiT3ch';
		$mail -> SMTPSecure = 'ssl';
		$mail -> Port = 465;

		$mail -> setFrom($_username, "Support/Inquiry");
		$mail -> addAddress($_username);
		// $mail -> addAddress('ellen@example.com');
		$mail -> addReplyTo($fromEmail, $name);
		// $mail -> addCC('cc@example.com');
		// $mail -> addBCC('bcc@example.com');

		// $mail -> addAttachment('/var/tmp/file.tar.gz');
		// $mail -> addAttachment('/tmp/image.jpg', 'new.jpg');
		$mail -> isHTML(true);

		$mail -> Subject = $subject;
		$mail -> Body = $contentBody;
		// $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';

		if (!$mail -> send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail -> ErrorInfo;
			return false;
		} else {
			echo 'Message has been sent';
			return true;
		}
	}

	private function baseUrl() {
		$path = dirname($_SERVER['SCRIPT_NAME']);
		$path = trim($path, '/');
		$baseUrl = Request::getUrl();
		$baseUrl = trim($baseUrl, '/');
		return $baseUrl . '/' . $path . ($path ? '/' : '');
	}

}

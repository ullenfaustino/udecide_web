<?php

namespace Helper;

use \Users;
use \User;

class FacebookHelper {

    public function shareOnFacebook() {
        $linkData = array('link' => "https://bitomi.tech", 
                            'message' => 'Join us now!');
        try {
            $response = FacebookHelper::facebook() -> post('/me/feed', $linkData, $_SESSION['facebook_access_token']);
            $graphNode = $response -> getGraphNode();
            return true;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e -> getMessage();
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e -> getMessage();
            return false;
        }
        // print_r($graphNode);
    }
    
    private function facebook() {
        $facebook = new \Facebook\Facebook( array('app_id' => FB_APP_ID, 'app_secret' => FB_APP_SECRET, 'default_graph_version' => 'v2.7', ));
        return $facebook;
    }

}
